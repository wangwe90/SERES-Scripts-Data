#!/bin/bash

source parameters.sh

cd replicates
qsub -V -t1-${number_of_replicates} posteriorsarray.qsub -F "${numstates}"
