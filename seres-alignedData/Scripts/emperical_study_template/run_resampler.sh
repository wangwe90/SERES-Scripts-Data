#!/bin/bash 

source parameters.sh

#Clear out the replicates directory
rm -f replicates/replicate* 2> /dev/null

#Run seres-resample
seres-resample alignment.fasta -d replicates -n $number_of_replicates \
    -l $length_of_replicates -s $seed 

