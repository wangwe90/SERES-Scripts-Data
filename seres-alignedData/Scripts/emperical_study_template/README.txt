This directory is a template for running an emperical study of SERES
performance. 

The basic procedure goes like this:
    1. move your alignment here and call it alignment.fasta.
    2. edit the run_resampler.sh script to specify the parameters you want.
    3. edit the posteriorsarray.qsub script in the replicates directory, make
       sure you give it enough resources.
    3. run the run_resmpler.sh script to create the replicates.
    4. run the launchjobs.sh script to launch all the hmmposterior jobs
    5. once the jobs are done, run: 
           posterioravg.py replicates > seres.pos 
       to get the seres results.
