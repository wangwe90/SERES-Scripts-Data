#!/usr/bin/env python
from ete3 import Tree
from sys import argv

#First, open the file the user specified
infile = open(argv[1])

#For each line in the file (genetree) strip off the partition length
tree_strings = []
for line in infile:
    tree_strings.append(line[line.find(']') + 1:]);

#For each tree string, read a tree using ete3
trees = []
for tree_string in tree_strings:
    trees.append(Tree(tree_string));

#Unroot all the trees
for index in range(len(trees)):
    trees[index].unroot()

#Function which checks a list of trees for topological breakpoints
def has_topo_breakpoint(trees):
    for i in range(0,len(trees) - 1):
        if(trees[i].robinson_foulds(trees[i+1], unrooted_trees=True)[0] != 0):
            return True

    return False

#Run the function and exit with the appropriate status code
if(has_topo_breakpoint(trees)):
    print("A topological breakpoint was found")
    exit(0)
else:
    print("A topological breakpoint was NOT found")
    exit(1)
