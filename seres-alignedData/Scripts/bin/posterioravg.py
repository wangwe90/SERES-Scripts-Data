#!/usr/bin/env python
from ete3 import Tree
from sys import argv
from sys import stderr
import glob

# Allows printing to stderr with the same syntax as print
def eprint(*args, **kwargs):
    print(*args, file=stderr, **kwargs)

# Accepts a pair of unrooted ete3 trees, returns true if they are topologically
# identical.
def are_topo_eq(tree_a, tree_b):
    return tree_a.robinson_foulds(tree_b, unrooted_trees=True)[0] == 0

# Function which looks in a particular path and returns a list of all files with
# a particular extention
def files_in_path_with_extention(path, extention):
    return glob.glob(path + '/*.' + extention)

# Parse a turnaround stirng into a tuple
def parse_turnaround_str(instr):
    lst = instr.strip().split(':')
    return (int(lst[0]), int(lst[1]), int(lst[2]), lst[3])

# Parse a keyfile into a list of turnaround points
def parse_keyfile(keyfile):
    keystr = keyfile.read()
    keystr.strip()
    keystr = keystr.replace(';', '')
    keystr = keystr[:-1]
    turnaround_strings = keystr.split(',')
    return [parse_turnaround_str(x) for x in turnaround_strings]

# Turnaround points are tuples (replicate_pos, original_pos, length, direction)

#Generator which accepts a list of turnaround points and spits out all the pairs
#of points which match.
def keypair_generator(turnaround_point_list):
    for tp in turnaround_point_list:
        replicate_pos = tp[0]
        original_pos = tp[1]

        for x in range(tp[2]):
            yield (replicate_pos, original_pos)
            replicate_pos += 1
            if(tp[3] == 'r'):
                original_pos += 1
            else:
                original_pos -= 1

#Parse a posterior file to a standard form
def parse_posterior_file(posterior_file):

    #List of trees which are supported
    tree_list = []

    #List of [position, [support_list]]
    supports = []

    #Read all the trees
    posterior_file.readline()
    for line in posterior_file:
        if line.strip() == '':
            break
        tree = Tree(line)
        tree.unroot()
        tree_list.append(tree)

    #Read all the posteriors
    posterior_file.readline()
    for line in posterior_file:
        line_lst = [x for x in line.strip().split(',') if x != '']
        entry = [int(line_lst[0]), [float(x) for x in line_lst[1:]]]
        supports.append(entry)

    return (tree_list, supports)

#Accept parsed posteriors and parsed keys, return posteriors where replicate
#positions are translated to original positions TODO test
def translate(parsed_posteriors, parsed_key):

    #Destructure the parsed posteriors
    tree_list, old_supports = parsed_posteriors
    new_supports = []

    position_pair_iterator = keypair_generator(parsed_key)
    pos_pair = next(position_pair_iterator)

    for (replicate_pos, support_list) in old_supports:
        while replicate_pos != pos_pair[0]:
            pos_pair = next(position_pair_iterator)
        new_supports.append([pos_pair[1], support_list])

    return (tree_list, new_supports)

#BEGIN BUSINESS LOGIC

#Validate the number of args and give the arg a more mneumonic name
if(len(argv) != 2):
    eprint("Error! Wrong number of args, exiting.")
    exit(1)
replicate_path = argv[1]

#Get a list of all the posteriors files and the key files
posterior_file_paths = files_in_path_with_extention(replicate_path, 'pos')
key_file_paths = files_in_path_with_extention(replicate_path, 'walk')

#Verify that the number of keys matches the number of posterior logs
if(len(posterior_file_paths) != len(key_file_paths)):
    eprint("Error! Number of posterior logs != Number of Key files, exiting.")
    exit(1)

#If we didn't find any files at all, that is also almost certainly unintended
if(len(posterior_file_paths) == 0):
    eprint("Error! No files found on that path, exiting.")
    exit(1)

posterior_file_paths.sort()
key_file_paths.sort()

#List of unique topologies we have encountered
topology_list = []

#Map of position -> # times that position is sampled
numsample_dict = {}

#Map of position -> total support at that position for each topology
support_dict = {}

#Loop through pairs of matching posterior logs and key files
for index in range(len(posterior_file_paths)):
    posterior_file = open(posterior_file_paths[index])
    key_file = open(key_file_paths[index])

    #Parse and translate the things
    parsed_posteriors = parse_posterior_file(posterior_file)
    parsed_keys = parse_keyfile(key_file)
    parsed_posteriors = translate(parsed_posteriors, parsed_keys)

    #Destructure the parsed posteriors tuple
    tree_list, supports = parsed_posteriors

    #Establish a map wich takes indicies into the posterior_trees and translates
    #into indicies in the list of already observed topologies
    tree_index_dict = {}
    for tree_index, tree in enumerate(tree_list):
        already_known = False
        for topo_index, topology in enumerate(topology_list):
            if are_topo_eq(topology, tree):
                tree_index_dict[tree_index] = topo_index
                already_known = True
                break
        if not already_known:
            topology_list.append(tree)
            tree_index_dict[tree_index] = len(topology_list) - 1
            for k,v in support_dict.items():
                v.append(0)

    #Loop throught the supports
    for position, support_list in supports:
        if position not in numsample_dict:
            numsample_dict[position] = 0
            support_dict[position] = [0 for x in topology_list]

        numsample_dict[position] += 1
        for index, support in enumerate(support_list):
            support_dict[position][tree_index_dict[index]] += support

#Output the topologies we encountered:
print('Topologies:')
for topology in topology_list:
    print(topology.write(format=9))
print()

#Output the support values
print("Position, Topologies") #TODO better header
for key in sorted(numsample_dict.keys()):
    line = [str(key)]
    for index, topo in enumerate(topology_list):
        line.append(str(support_dict[key][index]/numsample_dict[key]))
    print(', '.join(line))
