#!/usr/bin/env python

# This program produces a score for posterior distributions. The reported score
# is simply the average rf distance from the top supported topology to the true
# topology at each location.

# Usage: posteriorscore.py distribution.pos true.tree 

from ete3 import Tree
import sys

# Allows printing to stderr with the same syntax as print
def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

# Return a (partition_length, ete3 unrooted tree) tuple from a line from a
# genetree file
def parse_genetree_line(line):
    line = line.strip(';[')
    line = line.split(']')
    partition_length = int(line[0])

    tree = Tree(line[1])
    tree.unroot()

    return (partition_length, tree)

# Iterator generator for parsed genetrees
def parse_genetrees_file(genetrees_file):
    for line in genetrees_file:
        yield parse_genetree_line(line)

# Iterator generator for posterior logs, called with the posteriors file after
# the first section is read off. Yeilds a tuple (position, support_list)
def posteriors_gen(posteriors_file):
    for line in posteriors_file:
        line = line.strip()
        line = line.split(',')
        line = [x for x in line if x != '']
        yield (int(line[0]), [float(x) for x in line[1:]])

# Parse the posteriors file, returns a tuple of (list of topologies, iterator
# over posteriors)
def parse_posteriors_file(posteriors_file):

    #Grab the logged topologies
    topologies = []
    posteriors_file.readline()  #Read off header
    for line in posteriors_file:
        if(line.strip() == ''):
            break
        tree = Tree(line)
        tree.unroot()
        topologies.append(tree)

    #Create an iterator over the posteriors
    posteriors_file.readline()   #Second Header
    posteriors_iter = posteriors_gen(posteriors_file)

    #Return a tuple of our results
    return (topologies, posteriors_iter)


# BEGIN Business Logic
# Parse args
if(len(sys.argv) != 3):
    eprint("Wrong number of args!")
    exit(1)

# Open the posteriors file
posteriors_file = None
try:
    posteriors_file = open(sys.argv[1])

except FileNotFoundError:
    eprint("Could not open: \"" + sys.argv[1] + "\"")
    exit(1)

# Open the genetrees file
genetrees_file = None
try:
    genetrees_file = open(sys.argv[2])

except FileNotFoundError:
    eprint("Could not open: \"" + sys.argv[2] + "\"")
    exit(1)

#Parse the genetrees and posteriors files
genetrees_iter = parse_genetrees_file(genetrees_file)
posterior_topos, posterior_iter = parse_posteriors_file(posteriors_file)

#Used to log the average
total_rfdist = 0
num_positions = 0

for partition_length, true_topology in genetrees_iter:
    for x in range(partition_length):
        position, supports = next(posterior_iter)

        max_support_value = max(supports)
        max_support_index = supports.index(max_support_value)
        max_supported_topology = posterior_topos[max_support_index]

        rfdist = max_supported_topology.robinson_foulds(true_topology,
                unrooted_trees=True)[0]

        total_rfdist += rfdist
        num_positions += 1

print(total_rfdist/num_positions)
