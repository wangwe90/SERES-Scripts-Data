####   The main training/inference script for the recHMM.  Also directs where files get written to, etc ###
import sys
from numpy import *
from rpy import *
from felsenstein import *
from messages import *
from extra_functions import reversed
from optimize_tree import optimize_tree, get_data_constant
from HMM_algos import Forward, Backward, viterbi
from decoding import *
import os
import random
import copy
import time

trial=0
like=[[]] 
for i in range(100): #this is a stupid way to keep track of iteration likelihoods, maybe will change it eventually...
    like.append([])
def Baum_Welch(recobj, prefix='', it_max = 15, cutoff=50, logTrees = False, logBreaks = False, writeLog=False, steps=2,option='train', start1=None, start2=None, RealRecomb=None, trial=0, K=None, PlotPosts=False, AlignmentRegion=None, numSamples=50):


    if it_max!=15 and writeLog:
        sys.stderr.write('Performing at most '+str(it_max) +' EM iterations\n')
    if writeLog:
        sys.stderr.write('Region cutoff: '+str(cutoff)+'\n')
    try:
        sequences=recobj.alignment
    except:
        sequences=recobj
    like.append([])
    eps=.005
    N=len(sequences[:,1])
    M=len(sequences[1,:])
    transitions = initialize_transitions(K)
    if option=='train':
        if writeLog:
            sys.stderr.write('Initializing trees in HMM, taking best of '+str(numSamples)+' '+str(K)+' -tuples of trees\n')
        prob = -inf
        for i in range(numSamples):
            test_forest = get_distinct_trees(K, sequences, N)
            emissions = zeros((K,M))
            for k in range(K):
                emissions[k,:] = get_emissions(test_forest[k])
            forward, seq_prob=Forward(transitions, emissions, M)
            if seq_prob > prob or not i:
                forest, prob = test_forest, seq_prob
#             print 'Sequence prob for trial ', i, ': ',seq_prob
#             print 'Max prob so far:', prob
            
    else:
        forest=[tree(start_tree0,sequences), tree(start_tree1, sequences), tree(start_tree2, sequences)]
    if writeLog:
        sys.stderr.write('Finished with tree initialization\n')
    emits=zeros((K,M))
    for k in range(K):
        emits[k,:]=get_emissions(forest[k])
    emissions=emits
    old_seq_prob=-inf

    
    ###  Start iterative training of HMM ###
    iterations = 0
    totalTime = 0
    if not markov_test(transitions):
        sys.stderr.write('Transitions not summing to 1')
    while True:
        if writeLog:
            sys.stderr.write("transitions:\n "+str(transitions)+'\n')
        iter_begin=time.time()
        forward, seq_prob=Forward(transitions, emissions, M)

        diff=(seq_prob)-(old_seq_prob)
        if writeLog:
            if iterations>0:
                sys.stderr.write('Overall likelihood changed by: '+str(diff)+'\n')
            sys.stderr.write('Sequence probability: '+str(seq_prob)+'\n')
        like[trial].append(seq_prob)

        if diff<eps and diff>=0:
            like[trial].append(seq_prob)
            break
        if abs(diff/seq_prob)<.001:
            if writeLog:
                sys.stderr.write('convergence by fractional increase criterion\n')
            break
        
        backward, b_seq_prob=Backward(transitions, emissions, M)
        
        
        #check for oscillatory behaviour:
        try:
            x=like[trial][-1]-like[trial][-2]-(like[trial][-3]-like[trial][-4])
            if abs(x)<.01 and like[trial][-1]>like[trial][-2]:
                sys.stderr.write('Convergence deduced by oscillatory behaviour\n')
                like[trial].append(seq_prob)
                break
        except: # it's somewhat OK if something goes wrong here, usually among the first few iters, it can't
            # 'look back' that far
            pass
        
        ### Check if forward and backward recursions gave same sequence prob. IMPORTANT!  ###
        if abs(exp(seq_prob)-exp(b_seq_prob))>.01:
            sys.stderr.write('Forward, backward calculations are inconsistent!  Check calculations\n')
            
        ###  Calculate expected transition counts   ###
        Expected_transitions=zeros((K,K))
        total=zeros(K)
        for k in range(K):
            for l in range(K):
                Expected_transitions[k,l]=0
                for i in range(M-1):
                    Expected_transitions[k,l]=add_logs(Expected_transitions[k,l], forward[k,i]+log(transitions[k,l])+emissions[l,i+1]+backward[l,i+1]) 
                total[k]=add_logs(total[k],Expected_transitions[k,l]) # this eventually is the denominator in M-step

        ###  Maximize transition probabilities   ###
        Maximized_transitions=zeros((K,K))
        for k in range(K):
            for l in range(K):
                Maximized_transitions[k,l]=exp(Expected_transitions[k,l]-total[k]) 
        
        ###  Maximize "emission" probabilities, i.e. make better trees via S.EM  ###
        
        constant=1 
        if constant!=1: # for a bit I thought of setting this on the cmd line, but it is hard coded above
            sys.stderr.write('Scaling posteriors by constant: '+str(constant)+'\n')
        posteriors=zeros((K,M))
        for k in range(K):
            forest[k].posteriors=[]
            for m in range(M):
                post=(exp(forward[k,m]+backward[k,m]-seq_prob))#the prob. that column came from tree k
                forest[k].posteriors.append(post*constant)
                posteriors[k,m]=post*constant
        posterior_test(forest)

        if writeLog:
            breaktime = time.time()
            U,pointers = decoding3(posteriors,cutoff)
            K, M = shape(posteriors)
            Max_U = matrix_max(U[:,M-1,:])
            path= reversed(traceback(find_val(U,Max_U),pointers))
            breakpts=breaks(path)
            if 0 in breakpts:
                breakpts.remove(0)
            sys.stderr.write('Posterior decoding took: ' +str(time.time()-breaktime)+' seconds\n')            
            sys.stderr.write('Current predicted breakpoints: ' + str(breakpts) +'\n')


        Max_forest=[ [] for i in range(K)]
        for k in range(K): #optimize all k trees into Max_forest, scaling the expected counts by above posterior prob.
            now=time.time()
            forest[k].S, forest[k].fas = get_expected_counts(forest[k])
            # this is the main rate limiting step: we care about how long it takes...
            if writeLog:
                sys.stderr.write("Computing expected counts took "+str((time.time()-now)/60)+" minutes\n")
            Max_forest[k]=tree(optimize_tree( forest[k], sequences,steps ).adj, sequences) 
            forest[k]=None
        Maximized_emissions=zeros((K,M))
        for k in range(K):
            try:
                Maximized_emissions[k,:]=get_emissions(Max_forest[k])
            except ValueError:
                # print shape(Max_forest[k].seqs)
                # print shape(get_emissions(Max_forest[k])), "Shape mismatch Error in get_emissions()"
                return None 
                    
        ###  Update parameters   ###
        #Maximized_emissions=normalize_emissions(Maximized_emissions)
        transitions, emissions=Maximized_transitions, Maximized_emissions
        Maximized_emissions=None
        forest=Max_forest
        Max_forest=None
        old_seq_prob=seq_prob
        iterations+=1
        if not markov_test(transitions):
            sys.stderr.write( 'Transitions not summing to 1, after iterations: '+str(iterations) +'\n')
            ###  Loop until likelihood changes a small amount, then return maximized matrices
        if iterations>=it_max:
            break
        iterTime = (time.time()-iter_begin)/ float(60)
        totalTime += iterTime
        if writeLog:
            sys.stderr.write( 'Iteration ('+str(iterations+1)+') took '+str(iterTime)+ ' minutes\n')

    if not writeLog and PlotPosts:
        U,pointers = decoding3(posteriors,cutoff)
        K, M = shape(posteriors)
        Max_U = matrix_max(U[:,M-1,:])
        path= reversed(traceback(find_val(U,Max_U),pointers))
        breakpts=breaks(path)
        if 0 in breakpts:
            breakpts.remove(0)
        if M in breakpts:
            breakpts.remove(M)
    if PlotPosts:
        plot_posteriors(posteriors, iterations, breakpts, like[trial], trial, region=AlignmentRegion, pre=prefix)
    if writeLog:
        sys.stderr.write( "EM algorithm has converged.\n")
    if logTrees:
        # kliu START CODE CHANGE
        U,pointers = decoding3(posteriors,cutoff)
        K, M = shape(posteriors)
        Max_U = matrix_max(U[:,M-1,:])
        path= reversed(traceback(find_val(U,Max_U),pointers))
        breakpts=breaks(path)
        if 0 in breakpts:
            breakpts.remove(0)
        # kliu END CODE CHANGE
        # kliu - need to initialize breakpts prior to use
        # fixed bug here
        regions = [0]+breakpts
        f=open(prefix+'trees.txt','a')
        f.write('\nTrial '+str(trial)+'\n')
        for i,region in enumerate(regions):
            if i!=len(breakpts):
                start, end = region, regions[i+1]
            else:
                start,end = region, M
            region_tree = forest[path[start+1]]
            f.write('\nRegion '+str(start+1)+' '+str(end)+':\n')
            f.write(tree2dot(region_tree.adj,stringout=True))
        f.close()
    if not writeLog and not PlotPosts:
        U,pointers = decoding3(posteriors,cutoff)
        K, M = shape(posteriors)
        Max_U = matrix_max(U[:,M-1,:])
        path= reversed(traceback(find_val(U,Max_U),pointers))
        breakpts=breaks(path)
        if 0 in breakpts:
            breakpts.remove(0)
        if M in breakpts:
            breakpts.remove(M)    
    # print 'Final predicted breakpoint positions:\n ', tuple(breakpts)
    # print 'Final data likelihood: '+str(seq_prob)
    if logBreaks:
        pass
        # print("DEBUG: Logging Breakpoints")
        # f=open(prefix+'breakpoints.txt','a')
        # f.write('\n'+str(trial)+'\t'+str(seq_prob)+ '\t'+str(tuple(breakpts))+'\n')
        # f.close()
        # print("DEBUG: Done Logging Breakpoints")
    return seq_prob,transitions, emissions, forest, posteriors

def get_distinct_trees(k, seqs, N):
    while 1:
        foundSame=False
        forest=[tree(rand_tree(N), seqs) for i in range(k)]
        try:
           for i in range(k):
               for j in range(k):
                   if j>=i:
                       continue
                   else:
                       if not trees_distinct(forest[i], forest[j]): #avoid seeding with identical trees
                          foundSame=True
                          break
           if not foundSame:
               break
        except:
            # print "could not verify distinctive trees"
            break
    #print 'non-redundant seed forest generated'
    return forest

def posterior_test(forest):
    K=len(forest) # number of trees
    M=forest[0].M
    post_sums=[0 for i in range(M)]
    for m in range(M):
        for k in range(K):
            if type(forest[k])==list:
                continue
            post_sums[m]+=forest[k].posteriors[m]
    for sum in post_sums:
        if abs(sum-1)>.01:
            # print "Posteriors don't sum correctly!"
            return False
    return True
        
def normalize_emissions(matrix):
    array=copy.deepcopy(matrix)
    rows, cols=shape(array)
    total=[0,0] # for now, change for larger HMMs
    for row in range(rows):
        for col in range(cols):
            total[row]=add_logs(total[row], array[row, col])
    for row in range(rows):
        for col in range(cols):
            array[row, col]=array[row,col]-total[row]
    return array
def trees_distinct(tree1, tree2):
    'See if two trees are reasonably equal'
    if abs(tree1.likelihood()-tree2.likelihood())<.1:
        # print 'Identical trees randomly generated, making new ones...'
        return False
    else:
        return True
def markov_test(array):
    good=True
    rows, cols=shape(array)
    for row in range(rows):
        total=sum(array[row, :])
        if abs(total-1)>.001:
            good=False
    return good
def get_emissions(tree):
    return [tree.prob_column(m) for m in range(tree.M)]
def initialize_transitions(k):
    small=.0001/k
    big=1-small
    transitions=zeros((k,k))
    for i in range(k):
        for j in range(k):
            if i==j:
                transitions[i,j]=big
            else:
                transitions[i,j]=small
                
            
    return transitions

def hamming(vec1, vec2):
    diffs=0
    if len(vec1)!=len(vec2):
        pass
            # print 'not equal length vectors'
    for i,each in enumerate(vec1):
            if each==vec2[i]:
                    diffs+=1
    return diffs
def breaks(vector):
    breakpoints=[]
    for i in range(len(vector[1:])):
        if vector[i]!=vector[i-1]:
            breakpoints.append(i)
    return breakpoints
def plot_posteriors(posteriors,iteration, breakpts, like_in, trial=0, region=None, only_likelihood=False,pre=''):
    # todo: how much of this to cut out?
    x,y=posteriors[0,:], posteriors[1,:]
    if len(posteriors[:,1])==3:
        z=posteriors[2,:]
    if len(x)<=40:
        rtype='o'
    else:
        rtype='l'
    yrange=[0,1]
    # if not 'nfs' in os.getcwd():
#         target='/Users/oscar/mnt/program_outputs/Plots_Trial_%d' %(trial)
#     if 'nfs' in os.getcwd():
#         target='/nfs/users/oscar/program_outputs/Plots_Trial_%d' %(trial)
#     if not os.getcwd()==target:
#         if not os.path.exists(target):
#             os.chdir(target[:target.index('Plots')])
#             os.mkdir('Plots_Trial_%d' %(trial))
#             os.chdir(target)
    # first we (re-)plot the likelihood progression
    # todo remove this
#     likeH=copy.deepcopy(like_in)
#     if len(likeH)>1:
#         likeH.pop(0)
#     title='Likelihood of Alignment'
#     filename='posterior_iteration_likelihood.pdf'
#     if region!=None:
#         title+=', Region='+str(region)
#         filename=filename[:filename.index('.pdf')]+'_'+str(region[0])+'_'+str(region[1])+'.pdf'
#     r.pdf(filename, width=5, height=4)
#     r.plot(likeH, type='o', main=title, pch=2, col='blue',ylab='loglikelihood', xlab='Iteration')
#     r.dev_off()
#     toWrite=[]
#     nStates=len(posteriors[:,1])
#     for i in range(nStates):
#         toWrite+=list(posteriors[i,:])
#     toWrite=tuple(toWrite)
#     # write a copy of predicted breakpoints
#     # todo: keep this!


    # write a copy of the current posterior probabilities (for re-plotting in R)
    # todo add option for doing this
    
#     f=open('posteriorValues','a')
#     f.write('\n'+'posteriors'+str(iteration)+'=matrix(c'+str(toWrite)+',byrow=T,nrow='+str(nStates)+')'+'\n')
#     f.write('\n likelihood='+str(like_in[-1])+'\n')
#     f.close()
#     toWrite=None
#     if writeLog:
#         'Breakpoints predicted at: ', breakpts
#     if only_likelihood:
#         print "only likelihood plotted, in:", os.getcwd()
#         return 1

    #plotting current posterior distribution
    
    toWrite=[]
    nStates=len(posteriors[:,1])
    for i in range(nStates):
        toWrite+=list(posteriors[i,:])
    toWrite=tuple(toWrite)
    # write a copy of the current posterior probabilities (for re-plotting in R)
    f=open(pre+'posteriorValues.txt','a')
    f.write('\n'+'posteriors'+str(iteration)+'=matrix(c'+str(toWrite)+',byrow=T,nrow='+str(nStates)+')'+'\n')
    f.write('\n likelihood='+str(like_in[-1])+'\n')
    f.close()
    toWrite=None

    title='Posterior State Distribution, Likelihood: '+str(like_in[-1])
    filename=pre+'posteriors_trial_%d.pdf' %(trial)
    r.pdf(filename, width=6, height=3)
    r.barplot(posteriors, border='NA', space=0, main=title, xlab='Sequence position', col=['darkblue','darkgreen','black','purple', 'gray','yellow','orange','red','brown'], ylab='Posterior Probability')
    r.axis(side=1, tick=True, xaxp=[0,len(x),10])
    r.abline(v=breakpts,col='green',lty='dashed', lwd=2)
    r.dev_off()





likelihoods=[]
viterbis=[ ]






##baby_alignment=array([[3,0,2,1,1,3,2,1,1],[2,0,2,1,0,1,3,0,0],[1,0,0,3,0,1,3,0,0],[1,0,0,3,2,1,1,2,0]])
##baby_tree1=array([[0,0,0,0,0,0,2.5],[0,0,0,0,.25,0,0],[0,0,0,0,.25,0,0],[0,0,0,0,0,.5,0], [0,.25,.25,0,0,.25,0],[0,0,0,.5,.25,0,.25],[2.5,0,0,0,0,3,0]])
##
##baby_tree0=array([[0,0,0,0,.5,0,0],[0,0,0,0,.5,0,0],[0,0,0,0,0,.5,0],[0,0,0,0,0,.5,0],
##                    [.5,.5,0,0,0,0,2],[0,0,.5,.5,0,0,3],[0,0,0,0,2,3,0]])
##baby_trees=[tree(baby_tree0, baby_alignment), tree(baby_tree1, baby_alignment)]
# start_tree0=array([ [0,0,0,0,0,.1,0 ] , [0,0,0,0,0,.1,0], [0,0,0,0,.1,0,0] , [0,0,0,0,.1,0,0], [0,0,.1,.1,0,0,1], [.1,.1,0,0,0,0,1] ,[0,0,0,0,1,1,0] ])
# start_tree1=array([ [0,0,0,0,.1,0,0] ,[0,0,0,0,0,.1,0] ,[0,0,0,0,.1,0,0] ,[0,0,0,0,0,.1,0] ,[.1,0,.1,0,0,0,1],
# [0,.1,0,.1,0,0,1] ,[0,0,0,0,1,1,0] ] )

# start_tree2=array([ [0,0,0,0,0,.1,0] ,[0,0,0,0,.1,0,0] ,[0,0,0,0,.1,0,0] ,[0,0,0,0,0,.1,0] ,[0,.1,.1,0,0,0,1],
# [.1,0,0,.1,0,0,1] ,[0,0,0,0,1,1,0] ] )
foo=3
trial=0



#seq_prob,transitions, emissions, forest, posteriors=Baum_Welch(seqs
#likelihoods.append(seq_prob)
##print 'Viterbi path:'
##print viterbi(seqs, [.5,.5], transitions, emissions)
##print 'How many states predicted: ' , hamming(viterbis[trial], input_data.state_path)
##print viterbis[trial]    

