from numpy import *
import time
from math import log, exp
import copy
import random
##from matrix_exp import *
#from messages import *def Prob

sigma=[0,1,2,3]
def olog(x):
    if x<=0:
        print x
        print 'calling negative log, in felsenstein module'
    else:
        return log(x)
class return_object(object):
    def __init__(self,old_prob, tree):
        self.old_prob=old_prob
        self.tree=tree
        self.new_prob=None
        self.max_spanning=None
        self.old_adj=None
        self.adj=None
        self.W=None
    def print_orig(self):
        return tree2dot(self.old_adj)
    def print_new(self):
        return tree2dot(self.adj)

def o_exp(x):
    try:
        if x<-14:
            return 0
        else:
            return exp(x)
    except OverflowError:
        print "Overflow in o_exp: calling", x
        

def list_edges(Tree):
    'returns a list of node-pairs in a tree (as matrix)'
    nodes=len(Tree[:,1])
    edges=[]
    for i in range(nodes):
        if i ==nodes-1:
            break
        for j in range(nodes):
            if Tree[i,j]==0 or i==j:
                continue
            if not (i,j)  in edges:
                if not (j,i) in edges:
                    edges.append((i,j))
    return edges

def list_weighted_edges(Tree):
    nodes=len(Tree[:,1])
    edges=[[],[]]
    for i in range(nodes):
        if i ==nodes-1:
            break
        for j in range(i+1,nodes):
            if Tree[i,j]==0:
                continue
            edges[0].append(Tree[i,j])
            edges[1].append((i,j))
    return edges

def Prob(tree,a,b,t):
    """
    define evolution model
    """
    if t<=0:
        print 'Calling negative time point'
    alpha=.1
##    if a==b:
##        x= .25+.75*exp(-4*alpha*t)
##    else:if a==b:
##        x= .25+.75*exp(-4*alpha*t)
##    else:
##        x= .25-.25*exp(-4*alpha*t)
##    if x<=0:
##        print "Probability less than or equal to zero, may indicate problem. Tried to call:", t
##        x= .25-.25*exp(-4*alpha*t)
##    if x<=0:
##        print "Probability less than or equal to zero, may indicate problem. Tried to call:", t
    #return x
    return tree.model.hkyanalytic(a,b,t)
def prior(tree,a):
    #return .25
    return tree.model.prior[a]


def optimize_links(current_tree,length):
    """
    Input: 4-dimensional array of expected count values, and number of nodes
    Output: 2-dimensional array of optimized edge lengths for all pairs of edges,
    and a 2-dimensional array of maximized log-scores
    """
    global M,root
    root=current_tree.root
    M=length
    S=current_tree.S
    nodes=current_tree.nodes
    W=zeros((nodes,nodes))
    branch_lengths=zeros((nodes,nodes))
    choice='b'# raw_input("Do you want to do Newton's method or brute-force optimization? n/b: ")
    if choice=='n':
        tolerance=.01# input("Newton tolerance level: ")
    for i in range(nodes):
        for j in range(i,nodes):
            if i==j:
                continue
            #print 'trying to find',i,j
            
            if choice=='b':
                t_hat=brute_maximize(i,j, current_tree) #this is not so good, don't use
            elif choice=='n':
                t_hat=newton_maximize(i, j,tolerance, .1,current_tree) #the best value of
                if t_hat>=13:
                    print 'large, suspicious tHat encountered...'
##                    print 'large tHat:',t_hat, ' lets examine behaviour of likelihood scores:'
##                    for number in [1,1.5,2,4,5,7,9,10,15,20]+range(20,int(t_hat),5)+[t_hat]:
##                        print number, "score:", local(number,i,j,current_tree)
                    t_hat=find_comparable(t_hat, i,j, current_tree)
                    #print 'comparable? for', i,j,': ', t_hat, 'score:', local(t_hat, i,j, current_tree)
            #print 't hat for ',i,j, t_hat, 'score: ', local(t_hat,i,j,current_tree)
            branch_lengths[i,j]=branch_lengths[j,i]=t_hat
            W[i,j]=W[j,i]=local(t_hat,i,j,current_tree)
    S=None
    current_tree.S=None
    return W,branch_lengths

def find_comparable(t_hat, i,j,tree):
    "Find a comparable optimal time when branch length is longer than 20"
    original_value=local(t_hat,i,j,tree)
    suggestion=.1
    suggested_value=local(suggestion,i,j,tree)
    tol=.3
    while 1:
        if suggested_value>original_value:
            print 'Conducting systematic search for tHat'
            #do a brute-force search for the best value, which is probably small!
            x = .001
            y_max=-inf
            while 1:
                y=local(x,i,j,tree)
                if y>y_max:
                    y_max=y
                    x+=.001
                elif y_max>y:
                    return x
        if abs(original_value-local(suggestion, i,j,tree))<tol:
            break
        suggestion +=1
    return suggestion
        


def brute_maximize(i,j,tree):
    x_vect = [.01]
    y=[]
    x=.01
    while x<=14:
        x_vect.append(x)
        y.append(local(x,i,j,tree))
        x+=.05
        try:
            if y[-1]<y[-2]:
                t_max=x-.15
                if t_max<0:
                    #print 'Neg tMax: ', t_max
                    return .01
        except IndexError:
            continue
    y_max=max(y) 
    t_max=x_vect[y.index(y_max)]
    return t_max

def local(t,i,j,tree):
    if t<0:
        print 'calling negative-time point likelihood'
    ret=0
    for a in sigma:
        for b in sigma:
            ret+=olog((Prob(tree,a,b,t))/(prior(tree,a)))*tree.S[i,j,a,b]
    if not ret>1 and not ret<=1:
        print 'nan encountered in local function :', ret, tree.S[i,j,:,:]
    return ret
def newton_maximize(i,j,tolerance,x,tree):
    """
    Input: node i, node j, x=guess at branch length, S=count_array
    Output: the maximum value for branch length
    """
    if x<.001:
        print 'Very very small x passed to Newton maximizer'
    if i==root or j==root:
        if local(.01,i,j,tree)>local(.05,i,j,tree):
            #print 'small optimal length between',i,j
            return .001
    h=.001
    change=100
    fxph, fxmh, fx=local(x+h,i,j,tree), local(x-h,i,j,tree), local(x,i,j,tree)
    
    while change>tolerance:
        diff=((fxph-fx)/h)/((fxph-2*fx+fxmh)/(h**2))
        x-=(diff)
        if x<0:
            return .01
        if x<.001:
             print 'Very very small x passed to Newton maximizer'
             x+=.001
        new=local(x,i,j,tree)
        change=abs(fx-new)
        fxph, fxmh, fx=local(x+h,i,j,tree), local(x-h,i,j,tree), new
        #print 'x, change:', x, change
    if x<0:
        print 'Negative value has escaped to the outer level!'

    return x

            

def add_logs(x,y):
    "A fast way to add logarithms"
    if not x==0 and not y==0:
        return x+olog(1+o_exp(y-x))
    elif x==0 and y==0:
        print 'problem with log values'
        return 3500
    elif x==0:
        return y
    elif y==0:
        return x
    

def big(A,B,alph, t):
	return (-3*A*alph*o_exp(-4*alph*t))/(.25+.75*(-4*alph*t))+(B*alph*o_exp(-4*alph*t))/(.25-.25*o_exp(-4*alph*t))

def tree2dot(mat_input,option='max', stringout=False):
    """
    Outputs DOT code of the given tree
    """
    if type(mat_input)==tuple:
        mat_input=array(mat_input)
         
    if type(mat_input)==list:
        mat_input=array(mat_input)
    edges=list_edges(mat_input)
    output=''
    if not stringout:
        print "graph tree{"
    else:
        output="graph tree{"
    for edge in edges:
        row,col=edge
        if option!='max':
            if not stringout:
                print option+"%d" %row, "--",option+"%d" %col,'[label=',mat_input[row,col],'];'
            else:
                output+=option+"%d" %row + "--"+option+"%d" %col +'[label='+str(mat_input[row,col])+'];\n'
        else:
            if not stringout:
                print row,"--",col,'[label=',mat_input[row,col],'];'
            else:
                output+=str(row)+"--"+str(col)+'[label='+str(mat_input[row,col])+'];\n'
    if not stringout:
        print "}"
    else:
        output+="}"
    if stringout:
        return output

def make_tree(D,b_l):
    return make_trivalent(max_span_tree(D,b_l))

def max_span_tree(D,b_l):
    "Kruskal's algorithm for finding a maximum spanning tree"
    num_edges=0
    nodes=len(D[:,1])
    edges=[[],[]]
    C=range(nodes)
    newTree=identity(nodes)
    for i in range(nodes):
        if i ==nodes-1:
            break
        for j in range(i+1,nodes):
            edges[0].append(D[i,j])
            edges[1].append((i,j))
    while num_edges!=nodes-1:
        if 1:
            new=edges[0].index(max(edges[0]))
            i,j=edges[1][new]
            if C[i]!=C[j]:
                newTree[i,j]=newTree[j,i]=b_l[i,j]
                num_edges+=1
                to_change=[]
                for node in range(nodes):
                    if C[node]==C[i] and node!=j:
                        to_change.append(node)
                for node in to_change:
                    C[node]=C[j]
        edges[0].remove(edges[0][new])
        edges[1].remove(edges[1][new])
    return newTree



def is_trivalent(matrix,N,ideal):
    for row in range(len(matrix[:,1])):
        if len(list_rels(row,matrix))!=ideal[row]:
            return False
    return True
        
def make_trivalent(mat, log=False):
    if log:
        print 'doing new way'
    matrix=copy.deepcopy(mat)
    eps=.00001
    breakout=False
    node_purgatory=[]
    nodes=len(matrix[:,1])
    N=(nodes-1+2)/2
    ideal=[1 for i in range(nodes)]
    for index in range(N, nodes):
        ideal[index]+=2
    ideal[root]=2
    actual=[0 for i in range(nodes)]
    times_thru=0
    while not is_trivalent(matrix,N,ideal):
        times_thru+=1
        if times_thru==10 and node_purgatory==[root]:
            possible=list_rels(N,matrix)
            for internal in possible:
                if ideal[internal]==3:
                    break
            dist=matrix[N,internal]
            matrix[root, N]=matrix[N,root]=dist/float(2)
            matrix[root, internal]=matrix[internal,root]=dist/float(2)
            matrix[N,internal]=0
            matrix[internal, N]=0
        if times_thru==20:
            new_array=zeros(shape(matrix))
            size=len(matrix[:,1])
            for i in range(size):
                for j in range(size):
                    new_array[i,j]=int(matrix[i,j]!=0)
            print mat
            print new_array
        #print matrix
        for index in range(nodes):
                actual[index]=len(list_rels(index,matrix))
        for i in range(N, nodes):
            if actual[i]==1:
                if log:
                    print i, 'internal node that tries to be leaf'
                node_purgatory.append(i)
                matrix[i,:]=matrix[:,i]=0
                for index in range(nodes):
                    actual[index]=len(list_rels(index,matrix))
            elif actual[i]==2 and i!=root:
                if log:
                    print i,'internal node in a chain'
                node_purgatory.append(i)
                if log:
                    print list_rels(i,matrix)
                j,k=list_rels(i,matrix)[0],list_rels(i,matrix)[1]#trouble!!
                matrix[j,k]=matrix[j,i]+matrix[i,j]
                matrix[k,j]=matrix[j,k]
                matrix[i,:]=matrix[:,i]=0
                for index in range(nodes):
                    actual[index]=len(list_rels(index,matrix))
        for index in range(nodes):
                    actual[index]=len(list_rels(index,matrix))
        if node_purgatory==[]:
            break
        if log:
            print node_purgatory
        times=len(node_purgatory)
        for i in range(nodes):
            if times==0:
                break
            if actual[i]>ideal[i] and ideal[i]!=3:
                if log:
                    print i, 'a problem node, a leaf node'
                times-=1
                ip=node_purgatory.pop(0)
                neighbors=list_rels(i,matrix)
                if log:
                    print neighbors
                for node in neighbors[ideal[i]-1:]:
                    matrix[ip,node]=matrix[node,ip]=matrix[i,node]
                    matrix[i,node]=matrix[node,i]=0
                matrix[i,ip]=matrix[ip,i]=eps
            elif actual[i]>ideal[i] and ideal[i]==3:
                if log:
                    print i, 'a problem node, internal node'
                times-=1
                ip=node_purgatory.pop(0)
                neighbors=list_rels(i,matrix)
                distance=[]
                #neighbors.sort(key=hamming_dist)
                for node in neighbors[ideal[i]-1:]:
                    matrix[ip,node]=matrix[node,ip]=matrix[i,node]
                    matrix[i,node]=matrix[node,i]=0
                matrix[i,ip]=matrix[ip,i]=eps
                for index in range(nodes):
                    actual[index]=len(list_rels(index,matrix))
    return matrix
            


def list_rels(i, i_array):
    'lists the relatives of a given node in an adjacency matrix'
    neighbors=[]
    if type(i_array)==list:
        print 'calling list_rels on a list'
    for j,entry in enumerate(i_array[i,:]):
        try:
            if j in neighbors or entry==0 or i==j:
               continue
        except ValueError:
            print 'ValueError raised on :', j
        else:
            neighbors.append(j)
    return neighbors

def hamming_dist(j):
    seqs=bigseqs
    same=0
    for k in range(len(seqs[i,:])):
        if seqs[i][k]==seqs[j][k]:
            same+=1
    return same

def rand_tree(N, log=False):
    Pool=range(N)
    index=N
    adj=zeros((2*N-1,2*N-1))
    while len(Pool)!=2:
        if log:
            print 'Current pool:', Pool
        if index==N+2:
            index+=1
            if log:
                print 'encountered root node!'
        i=random.sample(Pool,1)[0]
        Pool.remove(i)
        j=random.sample(Pool,1)[0]
        Pool.remove(j)
        if log:
            print 'Next to join:', i,j
        adj[index,j]=adj[j,index]=adj[index,i]=adj[i,index]=.05
        Pool.append(index)
        index+=1
    i,j=Pool[0], Pool[1]
    root=N+2
    adj[root,i]=adj[i,root]=random.uniform(.5,1)
    adj[root,j]=adj[j,root]=random.uniform(.5,1)
    return adj
def rand_seqs(num,length):
    s=zeros((num,length))
    for i in range(num):
        s[i,:]=[random.randint(0,3) for j in range(length)]
    return s

def make_data(num,length):
    tree=rand_tree(num)
    seqs=rand_seqs(num,length)
    return tree,seqs
         

