def reversed(list):
    size=len(list)
    retlist=[0 for i in range(size)]
    for i,entry in enumerate(list): 
        retlist[size-1-i]=entry
    return retlist

'''
Needs to be re-written. Or just use default command line processing options.
Sigh.
What about - letter in argument options, e.g. -prefix option?
Fails that.
Check - starts an option, not contains it.
'''
def get_cmd_args():
    import sys
    import re
    argDict = {}
    skipNext = False
    for i,arg in enumerate(sys.argv):
        if not skipNext:
            # check for - at the *start*
            if re.search('^-', arg):
                # a new argument
                skipNext = True
                if not i==len(sys.argv)-1:
                    # check for - at the *start*
                    if not re.search('^-', sys.argv[i+1]):
                        argDict[arg.replace('-','')] = sys.argv[i+1]
                    else:
                        skipNext = False
                        argDict[arg.replace('-','')] = arg.replace('-','')
                else:
                    argDict[arg.replace('-','')] = arg.replace('-','')
        else:
            skipNext = False
            continue
    return argDict

