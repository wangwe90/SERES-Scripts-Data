#!/usr/bin/python
from numpy import *
from messages import tree2newick
import sys
import os
from extra_functions import get_cmd_args
from alignment2dict import get_fasta

usage = '''
General Usage:
./runTraining.py alignmentFile.fa > breakpoint_file

Options:
-lt output a tree file upon convergence, with each tree-state in newick format  to trees.txt
-k INT specify the number of HMM states, default = 2
-s INT specify the number of initial samples when randomly initializing states
-log print log messages while program runs
-lb log breakpoints to breakpoints.txt
-t INT number of trials to run.  t independent EM-runs will be done one after another
-p plot the posterior distribution upon convergence, as posterior_trial_$trial.pdf
-c INT set the minimum length cutoff for a recombinant region (default 50)
-max_iters INT set the maximum number of EM iterations allowed.
-prefix PATH , where to put output files
'''

if len(sys.argv)>1:
    # set default values:
    lt, k, numSamples_input, Log, t, p, c, max_iters, lb, path = False, 2, 20, False, 1, False, 50, 15, False,''
    cmdArgs = get_cmd_args()
    if 'h' in cmdArgs.keys():
        print usage
        sys.exit(0)
    if cmdArgs.has_key('t'):
        t = int(cmdArgs['t'])
    if cmdArgs.has_key('c'):
        c = int(cmdArgs['c'])
    if cmdArgs.has_key('lt'):
        lt = True
    if cmdArgs.has_key('k'):
        k = int(cmdArgs['k'])
    if cmdArgs.has_key('s'):
        numSamples_input = int(cmdArgs.get('s'))
    if cmdArgs.has_key('log'):
        Log = True
    if cmdArgs.has_key('p'):
        p = True
        from rpy import *
        if Log:
            sys.stderr.write('Imported Rpy library for plotting functions\n')
    if cmdArgs.has_key('lb'):
        lb = True    
    if cmdArgs.has_key('maxiters'):
        max_iters = int(cmdArgs['maxiters'])
    if cmdArgs.has_key('prefix'):
        path = cmdArgs['prefix']
    # print "Prefix is ", path, "."
try:
    aligndict=get_fasta(sys.argv[1], log=False)
# parse fasta file into dictionary
except:
    print "Problem: Fasta File could not be imported. Check file location (given as: ", sys.argv[1], "), and format."
    print usage
    sys.exit()
 

if Log:
    sys.stderr.write('Parsed fasta file:\n')
    for i,species in enumerate(aligndict.keys()):
        aligndict[species]=aligndict[species].replace('\n', '')
        sys.stderr.write('Sequence '+str(i)+": "+species+'\n')
numtaxa=len(aligndict.keys())
# make an array of digits instead of letters
alignArray=[ [] for i in range(numtaxa) ]
for i, species in enumerate(aligndict.keys()):
    for letter in aligndict[species]:
        if not letter in ['A','C','G','T','-']:
            alignArray[i].append(4)
            sys.stderr.write('unknown letter encountered: '+letter+'\n')
        else:
            alignArray[i].append(['A','C','G','T','-'].index(letter))
## Remove gaps...if is deemed necessary ## NOTE, USUALLY THIS ISN'T DONE ANYMORE
def gap_stripper(alignArray):
    for i,species in enumerate(alignArray):
        for j,letter in enumerate(species):
            if letter==4:
                #print "Found gap...deleting column..."
                for k,specys in enumerate(alignArray):
                    alignArray[k][j]=-1
    for i,species in enumerate(alignArray):
        while -1 in species:
            alignArray[i].remove(-1)
    return alignArray

alignment=array(alignArray)

if Log:
    sys.stderr.write("Running training on alignment of size: "+ str(shape(alignment))+'\n')
if lt:
    os.system('rm -f '+path+'trees.txt; touch '+path+'trees.txt')
if lb:
    os.system('rm -f '+path+'breakpoints.txt; touch '+path+'breakpoints.txt')
    f= open(path+'breakpoints.txt','a')
    f.write('Trial\tLikelihood\tBreakpoints')
    f.close()
from training import *
for i in range(t): # ACTUALLY RUN THE TRAINING/INFERENCE ALGORITHM
    # Baum_Welch(alignment, logBreaks = lb, it_max = max_iters, logTrees = lt, trial=i, K=k, numSamples=numSamples_input, writeLog=Log,PlotPosts=p,cutoff=c, prefix = path)
    #BEGIN change by Jsmith

    #Run Baum_Welch and capture the return values
    seq_prob,transitions, emissions, forest, posteriors = Baum_Welch(alignment, logBreaks = lb, it_max = max_iters, logTrees = lt, trial=i, K=k, numSamples=numSamples_input, writeLog=Log,PlotPosts=p,cutoff=c, prefix = path)

    #Print out all the trees
    print("Trees:")
    for elem in forest:
        speciesList = [ name.split()[0] for name in aligndict.keys()]
        print(tree2newick(elem, speciesList))
    print('')

    #Write out a header for the positions an posteriors
    numtrees=posteriors.shape[0]
    numsites=posteriors.shape[1]
    line = 'Position, '
    for number in range(1,numtrees + 1):
        line += 'Posteriors' + str(number) + ', '
    print(line[0:-1]  )

    #Write out all the positions with all the posteriors
    for position in range(0,numsites):
        line = str(position) + ', '
        for treenum in range(0, numtrees):
            line += str(posteriors.item((treenum, position))) + ', '
        line = line[0:-1]
        line += ''
        print(line)

    #END change by Jsmith

if lt:
    speciesList = [ name.split()[0] for name in aligndict.keys()]
    cmd = 'mv ' +path+'trees.txt '+path+'trees.dot;'+sys.path[0]+'/dot2newick.py '+path+'trees.dot '
    for name in speciesList:
        cmd+=name+' '
    cmd += ' >'+path+'trees.txt;rm '+path+'trees.dot'
    os.system(cmd)
    
    
