%module memory_tree
%include stl.i

%{
#include "memory_tree.h"
%}
namespace std {
  %template(vectori) vector<int>;
  %template(vectord) vector<double>;
   %template(vectorv) vector< vector<double> >;
   %template(vectorvv) vector< vector< vector<double> > >;
   %template(vectorl) vector< vector< vector< vector<double> > > >;
   %template(vectors) vector< vector< vector< vector< vector<double> > > > >;
};

void dovec(std::vector<double> vec);

std::vector< std::vector< std::vector< std::vector<double> > > > import_and_count(std::vector< std::vector<double> > matrix_input, int M_input, int root_input, std::vector< std::vector<std::vector<double> > > F_input,std::vector< std::vector<std::vector<double> > > E_input, std::vector<int> parent_input, std::vector<int> sibling_input, std::vector< std::vector< std::vector< std::vector<double> > > > S_input, std::vector< std::vector< std::vector< std::vector<double> > > > Sp_input, std::vector<double> post_input);
