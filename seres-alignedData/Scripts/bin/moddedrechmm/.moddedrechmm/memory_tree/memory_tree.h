/*
 *  tree.h
 *  count_traversal
 *
 *  Created by Oscar Westesson! on 12/14/07.
 *  Copyright 2007 __OscarWestesson__. All rights reserved.
 *
 */
#include<vector>

void dovec(std::vector<double> vec);
std::vector< std::vector< std::vector< std::vector<double> > > > import_and_count(std::vector< std::vector<double> > matrix_input, int M_input, int root_input, 
					   std::vector< std::vector< std::vector<double> > > F_input, 

					   std::vector< std::vector< std::vector<double> > > E_input, 
					std::vector<int> parent_input, 
					std::vector<int> sibling_input,
					std::vector< std::vector< std::vector< std::vector<double> > > > S_input, 
					std::vector< std::vector< std::vector< std::vector<double> > > > Sp_input, 
																							   std::vector<double> post_input);
class tree{
public:
	int nodes;
	int root;
	int M;
	int sigma;
	std::vector<int> parent;
	std::vector<int> sibling;
	std::vector<double> prior;
	std::vector<double> posteriors;
	std::vector< std::vector< std::vector< std::vector<double> > > > S;
	std::vector< std::vector< std::vector< std::vector<double> > > > Sp;
	std::vector< std::vector< std::vector<double> > > F;
        std::vector< std::vector<double> > G;
	std::vector< std::vector< std::vector<double> > > E;
	std::vector< std::vector<double> > adj;
	void traversal(void);
	std::vector<int> get_child(int i);
	double get_count(int m, int i,int j,std::vector<int> history);
	double prob_column(int m);
	double joint_prob(int m,int i,int j,int a,int b);
	double node_prob(int m,int i ,int a);
	std::vector< std::vector<double> > get_G_dp(int m);
	void get_G(int m);
	// Constructor: bring in inputs into object
	tree(void);
	tree(std::vector< std::vector<double> > matrix_input, int M_input, int root_input, 
		 std::vector< std::vector< std::vector<double> > > F_input, 
		
		 std::vector< std::vector< std::vector<double> > > E_input, 
		 std::vector<int> parent_input, 
		 std::vector<int> sibling_input, 
		 std::vector< std::vector< std::vector< std::vector<double> > > > S_input, 
		 std::vector< std::vector< std::vector< std::vector<double> > > > Sp_input, std::vector<double> post_input){
		
		S=S_input;
		posteriors=post_input;
		Sp=Sp_input;
		adj=matrix_input; 
		M=M_input; 
		root=root_input; 
		F=F_input; 

		E=E_input; 
		parent=parent_input; 
		sibling=sibling_input;
		nodes=adj[0].size();
		sigma=4;
		//double S[nodes][nodes][sigma][sigma];
		
		
		// initialize S:
//		for (int i=0; i<nodes; i++){
//			for (int j=0; j<nodes; j++){
//				for (int a=0; a<sigma; a++){
//					for (int b=0; b<sigma; b++){
//						S[j][j][a].push_back(0);
//					}
//				}
//			}
//		}
					
	
		}
	
};

	
