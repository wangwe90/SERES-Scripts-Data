/*
 *  tree.cpp
 *  count_traversal
 *
 *  Created by Oscar Westesson on 12/14/07.
 *  Copyright 2007 __OscarWestesson__. All rights reserved.
 *
 */
#include <iostream>
#include<queue>
#include <stack>
#include <list>
#include <vector>
#include <math.h>
#include "memory_tree.h"
using namespace std;
double Prob_evolve(int, int, double);
bool contains(vector<int>, int);
bool is_transition(int, int);
void print_vector(vector<int>);
void dovec(vector<double> vec);
double add_logs(double, double);
std::vector< std::vector< std::vector< std::vector<double> > > > import_and_count(std::vector< std::vector<double> > matrix_input, int M_input, int root_input, 
					   std::vector< std::vector< std::vector<double> > > F_input, 
					   std::vector< std::vector< std::vector<double> > > E_input, std::vector<int> parent_input,std::vector<int> sibling_input,										 std::vector< std::vector< std::vector< std::vector<double> > > > S_input,
  																   std::vector< std::vector< std::vector< std::vector<double> > > > Sp_input,
																				std::vector<double> post_input);


std::vector< std::vector< std::vector< std::vector<double> > > > import_and_count(std::vector< std::vector<double> > matrix_input, int M_input, int root_input, 
					   std::vector< std::vector< std::vector<double> > > F_input, 

					   std::vector< std::vector< std::vector<double> > > E_input, std::vector<int> parent_input, std::vector<int> sibling_input,
																				 std::vector< std::vector< std::vector< std::vector<double> > > > S_input,
																				std::vector< std::vector< std::vector< std::vector<double> > > > Sp_input,
																				std::vector<double> post_input)
{
	tree a(matrix_input, M_input, root_input, F_input, E_input, parent_input, sibling_input, S_input, Sp_input, post_input);
	//std::cout<<"Tree constructed within function.  Let's compute some things...\n";
	//std::cout<<"Using memory-conserving module\n";
        a.prior.push_back(.3);a.prior.push_back(.2);a.prior.push_back(.2);a.prior.push_back(.3);
	a.traversal();
	return a.Sp;
}

void dovec(vector<double> vec){
	for (int i=0; i<vec.size(); i++){
		cout<<vec[i]<<"  ";
	}
	cout<<"\n";
}
tree::tree(void){
	cout<<"constructor! Under void input\n";
	root=5;
}

//Probability Functions
double tree::node_prob(int m, int i, int a){
	return exp(F[m][i][a]+G[i][a]-prob_column(m));
}
double tree::joint_prob(int m, int i, int j, int a, int b){
	if (!adj[i][j])cout<<"Calling joint probability of non-tree edge. Invalid.\n";
	//cout<<"joint Prob called"<<i<<" "<<j;
	
	//cout<<"sibling of "<<i <<": "<<sib<<endl;
	if (parent[i]==j){
	//cout<<"flipped"<<i<<" "<<j<<"  \n";
		//flip i and j and also a and b!!
		int temp1=i;
		int temp2=j;
		i=temp2;
		j=temp1;
		int tempa=a;
		int tempb=b;
		a=tempb;
		b=tempa;
		
		
	}
	int sib=sibling[j];
	//cout<<"Calling: "<<" "<<i<<" "<<j<<" "<<a<<" "<<b<<endl;
	//cout<<"Sib: "<<sib<<endl;
	//if (G[m][i][a]*Prob_evolve(a,b,adj[i][j])*F[m][j][b]*E[m][sib][a]/prob_column(m)<.01)cout<<"Near Zero Prob";
	return Prob_evolve(a,b,adj[i][j])*exp(G[i][a]+F[m][j][b]+E[m][sib][a]-prob_column(m));
}

double add_logs(double x, double y){
    if (x!=0 && y!=0) return x+log(1+exp(y-x));
	
	else if (x==0 && y==0){
			cout<<"problem with log values";
		return 3500;}
	else if (x==0) return y;
	else if (y==0) return x;
}
	
double tree::prob_column(int m){
	//keep this in log-space, likely underflow location
	int sigma=4;
	double prior[4]={.35,.19,.25,.208};
	double sum=0;
	for (int a=0;a<sigma;a++){
		sum=add_logs(sum, log(prior[a])+F[m][root][a]);
	}
	
	if (sum>0) cout<<"Invalid Column probability outputted!\n";
	return sum;
}

// Depth-first tree-traversal
void tree::traversal(void)
{

	vector<int> k_vect, kids; 
	int i, count=0;
        vector<int> covered_nodes;
	stack<int> node_stack;
	//cout<<"beginning...\n";
	int start, new_node;
	//Do peeling and depth-first for each position 0<=m<M:
	//	std::cout<<"about to request G\n";
	for (int m=0; m<M; m++){
	  G=get_G_dp(m);
	
	for (count=0; count<nodes; count++){
		vector<int> history(nodes,-1);
		covered_nodes.clear();
		start=count;
	//cout<<"Start: "<<count<<endl;
	node_stack.push(count);
	while (true){
		if (node_stack.empty()){//cout<<"Stack empty, tree traversed completely\n"; 
			break;}
		else{
			new_node=node_stack.top();node_stack.pop();
			//cout<<"The new node: "<<new_node<<endl;
			//new_node=5

		if (new_node!=count){
				//cout<<"Doing something\n";
				//cout<<"We use "<<history.at(new_node)<<" to compute"<<" ("<<start<<","<<new_node<<") "<<endl;
			get_count(m,start,new_node,history);
				}

			covered_nodes.push_back(new_node);
			

				kids=get_child(new_node);
				for (i=0; i<kids.size(); i++){
					if (contains(covered_nodes, kids[i])) continue; //add only the 'children' that haven't been covered
					else{
						history[kids[i]]=new_node; //keep track of how the depth traversal proceeded, for jointProbs
						node_stack.push(kids[i]);
					
					}
				}
			}
		
		}
	}
	
	//Sum out over positions M, into Sp:
	for (i=0; i<nodes; i++){
		for (int j=0; j<nodes; j++){
			for (int a=0; a<sigma; a++){
				for (int b=0; b<sigma; b++){
	  			  Sp[i][j][a][b]+=S[i][j][a][b]*posteriors[m];
				}
			}
		}
	}

		
	}	
	//cout<<"\nEnd of tree traversal\n";
	return;}

//Returns a vector of relatives: either 1,2, or 3 long
vector<int> tree::get_child(int i){
	int j, count, arrsize;
	arrsize=adj[0].size(); //get the size of the array
	std::vector<int> k_vect;
	for (j=0; j<arrsize; j++){
		if (adj[i][j]){k_vect.push_back(j); //look for nonzero array entries
		}
	}
	count=k_vect.size();
	if (count==1){ //leaf node
		return k_vect;}
	else if (count==2 || count==3){
		return k_vect;
	}
}

double tree::get_count(int m, int i, int j, std::vector<int> history){
	//does the actual probability calculation
	int sigma=4;
	if (adj[i][j]){
		//cout<<"Tree Edge\n"; 
	
		for (int a=0; a<sigma; a++){
			for (int b=0; b<sigma; b++){
				
				  S[i][j][a][b]=joint_prob(m,i,j,a,b);
				
			}
		}
	}
	
	else { 
		if (j<i){ //utilize symmetry, just copy already-completed entries
			for (int a=0; a<sigma; a++){
				for (int b=0; b<sigma; b++){
					
						S[i][j][a][b]=S[j][i][b][a];
					
				}
			}
		}
		if (i<j){
			//cout<<"Non-Tree Edge\n";
			int c;
			int k=history.at(j);
			if (!k)cout<<"NO HISTORY RECORDED\n";
			//sum out over intermediate node
			for (int a=0; a<sigma; a++){
				for (int b=0; b<sigma; b++){

						S[i][j][a][b]=0;
						for (c=0; c<sigma; c++){
		       S[i][j][a][b]+=S[i][k][a][c]*joint_prob(m,j,k,b,c)/node_prob(m,k,c);
						}
					}
				}
			}
		}
		
	return 1.0;
}



void print_vector(vector<int> V){
	cout<<"The vector contains: ";
	for(int i=0; i<V.size(); i++) cout<<V[i]<<"\t";
	cout<<"\n";
	return;
}



bool is_transition(int i, int j){
	if (i==0){
		if (j==2) return true;
		else{return false;}
	}
	if (i==1){
		if (j==3)return true;
		else{return false;}
	}
	if (i==2){
		if (j==0)return true;
		else{return false;}
	}
	if (i==3){
		if (j==1)return true;
		else{return false;}
	}
}
double Prob_evolve(int i, int j, double t){
	
	// Implements the analytic solution to HKY85 evolution model
	//  A 0   C 1  G 2  T 3
	double alpha=.039;
	double beta=.00756;
	double prior[4]={.35,.19,.25,.208};
	double Pi_pur=prior[0]+prior[2];
	
	vector<int> purines;
	purines.push_back(0); purines.push_back(2);
	double Pi_pyr=prior[1]+prior[3];
	
	double mu,kappa;
	mu=alpha;
	kappa=beta/alpha;
	if (i==j){ //stay same nucleotide
		//cout<<"Staying Same nucleotide\n";
		if (contains(purines, i)){
			//cout<<"Purine stays same";
			return prior[j]+prior[j]*((1/Pi_pur) -1)*exp(-mu*t)+((Pi_pur-prior[j])/Pi_pur)*exp(-mu*t*(1+Pi_pur*(kappa-1)));
		}
		else { //i in pyrimidines:
			//cout<<"Pyrimidine stays same\n";
			return prior[j]+prior[j]*((1/Pi_pyr) -1)*exp(-mu*t)+((Pi_pyr-prior[j])/Pi_pyr)*exp(-mu*t*(1+Pi_pyr*(kappa-1)));
		}
	}
	if (i!=j){
		//cout<<"Base change";
		if (is_transition(i,j)){
			//cout<<"A transition:\n";
			if (contains(purines,j)){
				//cout<<j<<" is a purine, 0 or 2\n";
				return prior[j]+prior[j]*((1/Pi_pur)-1)*exp(-mu*t)-(prior[j]/Pi_pur)*exp(-mu*t*(1+Pi_pur*(kappa-1)) );
			}

		else if(!contains(purines,j)){ //j in pyrimidines:
			//cout<<j<<" is a pyrimidine, 1 or 3\n";
			return prior[j]+prior[j]*((1/Pi_pyr)-1)*exp(-mu*t)-(prior[j]/Pi_pyr)*exp(-mu*t*(1+Pi_pyr*(kappa-1)) );
		}
		}
			 
			 
	
		else if (!is_transition(i,j)){ 
			//cout<<"A transversion\n";
			return prior[j]*(1-exp(-mu*t)); }
	}
}

bool contains(vector<int> V, int target){ // Boolian is target contained within V
	for (int i=0; i<V.size(); i++){
		if (V[i]==target)return true;
	}
	return false;
}

vector< vector<double> > tree::get_G_dp(int m){
  int a;
  vector< vector <double> > Gm(nodes,vector<double>(4));
  //double Gm[nodes][4];
  //std::cout<<Gm.size()<<prior.size()<<endl;
  //   std::cout<<"beginning of function\n";
  for (int a=0; a<4; a++){ Gm[root][a]=log(prior[a]);}
  vector<int> kids = get_child(root);
//   for (a=0; a<kids.size(); a++){
//     std::cout<<kids[a];
//   }
//   std::cout<<endl;
  
queue<int> cue;
  for (int a=0; a<kids.size(); a++){
    cue.push(kids[a]);
  }
  int rlog=1;
  vector<int> covered_nodes;
  covered_nodes.push_back(root);
  //  std::cout<<"so far, so good!\n";
  while (1){
    if (cue.size()==0){
      // std::cout<<"Queue empty, finished!";
     break;
    }
    else{
      int c=cue.front(); cue.pop(); int p=parent[c]; int s=sibling[c];covered_nodes.push_back(c);
      //if (rlog){std::cout<<c<<" is the new node"<<endl;}
      for (int b=0; b<sigma; b++){
	Gm[c][b]=0;
	for (a=0; a<sigma; a++){
	  Gm[c][b]=add_logs(Gm[c][b], Gm[p][a]+log(Prob_evolve(a,b,adj[c][p]))+E[m][s][a]);
	}
      }
      kids=get_child(c);
      if (kids.size()==0)continue;
      else{
	//	std::cout<<"Adding children, a good sign!\n";
	for (a=0; a<kids.size(); a++){
	  //if (rlog){std::cout<<kids[a]<<' is the new child added'<<endl;}
	  if (contains(covered_nodes,kids[a]))continue;
	  else{
	    cue.push(kids[a]);
	  }
	}
      }
    }
  }
  //  std::cout<<"Traversal finished, let's compare answers...\n";
  // for (int b=0; b<nodes; b++){
//     for (a=0; a<sigma; a++){
//       if ((G[m][b][a]-Gm[b][a])>.01 || (G[m][b][a]-Gm[b][a])<-.01 ){
// 	cout<<"Strange behaviour: "<<b<<' '<<a<<' '<<(G[m][b][a]-Gm[b][a])<<endl;
//       }
//     }
  return Gm;
}
