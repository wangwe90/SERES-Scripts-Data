### Parse a fasta file into a dictionary of sequences ###
import sys

def get_fasta(fasta_input, log=True):
    if isinstance(fasta_input, (file)):
        fasta = fasta_input
    elif isinstance(fasta_input, (str)):
        fasta = open(fasta_input)
    else:
        print type(fasta_input), ': unknown filetype!'
    alignmentStringList=[line.strip() for line in fasta.readlines()]
    fasta.close()
    alignDict={}
    current_taxa=None
    for line in alignmentStringList:
        if len(line)==0:
            continue
        elif line[0]=='>':
            if log:
                print "New taxa:", line[1:]
            alignDict[line[1:]]=''
            current_taxa=line[1:]
        else:
            if log:
                print "Adding to taxa:", current_taxa
            alignDict[current_taxa]+=line
#         else:
#             print "Strange character in input file:", line
    return alignDict
#a=get_fasta(raw_input('Enter filename: '), log=True)
#print "Taxa:", a.keys()
#print "Sequences: ", a.values()
#get_fasta(sys.stdin)
