### Matrix Exponentiation / Substitution model ###

#import Numeric
from copy import deepcopy
from math import exp, log
import random
#from rpy import *

data={}
data['DroPer_CAF1']='GTTTCATACGAATTATAAACATTGGTATCTTAGTACCGTCTTTACTTTTATAAAATATCTGTTCGACA'
data['DroPer_CAF1']+='ATGTAGTTTTCTCGACAGAAACCTTCCAAGTTGAGTTTGATTTCGCGAAACACTTTTGGCATGATCTC'
data['DroPer_CAF1']+='TGGGGTCTTAAAATCATATTGATAAATACATCCAGGACTCAAAAAGGAAGAAAAGTTATAAAAAATCT'
data['DroPer_CAF1']+='CTGAATACTTCTTCTCGCCTGAAGTTCCTACAATGGTACCGATATCTAAGTCGAATACACGCAATAGT'
data['DroPer_CAF1']+='TTTCCAGTTCGAAGACAATTGGCTTGCA'


data['DroWil_CAF1']='TCTTCTTACGAATTATAAACATAGGAATTTTAGTACCATCCTTACTGGTATAAAATATTTGTTC'
data['DroWil_CAF1']+='AACTTTATAGTCTTCTCGATTAAATCCATTTAAGTTTATTTTTATTTCTCGAAATACATCTGGCTGACGATC'
data['DroWil_CAF1']+='AGGGTTATTAAAGTCGTAATGGTAAATTGTTCCCGGGCTCAAAAATGAGGCAAAATTGTAAAAAAATTCAG'
data['DroWil_CAF1']+='AATGCTTTTTTTTTCCAGAAGTTCCAACGATGGTTCCAATATCCAAGTCAAATGAGCAAAATAGCTCCCCCG'
data['DroWil_CAF1']+='TTTGAAGTAAGTTAGCTTGCA'

def make_zero(N):
    """
    Makes an empty list of lists, size N
    """
    R=[[0 for i in range(N)] for i in range(N)]
    return R

def make_diag(vector, option='array'):
    """
    Creates a list-list with vector as the diagonals.  Or, uses the built in Numeric funtion to do this
    """
    R=make_zero(len(vector))
    for i in range(len(vector)):
        R[i][i]=vector[i]
    if option=='list':
        return R
    elif option=='array':
        return R
    
def get_HKY85_matrix(data, alpha, beta):
    """
    Builds the HKY85 rate matrix from the nucleotide frequencies and input alpha, beta
    """
    ###  Determine nucleotide composition ###
    nucs=['A','C','G','T']
    totals=[0, 0, 0, 0]
    total=0
    #get the data in the right form...
    if type(data)!=dict:
        new_data={}
        for row in range(len(data[:,1])):
            new_data[row]=data[row,:]
    else:
        new_data=data
    for seq in new_data.values():
        #figure out data format, be it strings or numbers.  Numbers are a little easier to work with...
        for nucleotide in seq:
            if nucleotide!=4:
                if type(nucleotide)==str:
                    base=nucs.index(nucleotide)
                elif type(nucleotide)==int:
                    base=nucleotide
                else:
                    base=int(nucleotide)
                try:
                    totals[base]+=1
                    total+=1
                except:
                    print "Problem:", base
    ratios=[totals[i]/float(total) for i in range(4)]
    #print ratios    
    #transitions vs transversions:
    groups=[(1,3),(3,1),(0,2),(2,0)]
    ###  define rate matrix R  ###
    R=make_zero(4)  #this is a list-list and not an array; becuase of a python quirk (Numpy-Numeric disagreements)
    for row in range(4):
        for col in range(4):
            if (row,col) in groups:
                R[row][col]=ratios[col]*alpha
            else:
                if row==col:
                    continue
                R[row][col]=ratios[col]*beta
    for i in range(4):
        R[i][i]=-sum(R[i])
    return R,ratios

def exp_diag(mat,t):
    """
    Returns a diagonal matrix with the diagonal elements taken exp()
    """
    m=len(mat)
    array=[ [] for i in range(m)]
    for i in range(m):
        try:
            
            if not i in (0,m-1):
                array[i]= (mat[i][:i]+ (exp(mat[i][i]*t),)+ mat[i][i+1:])  
            elif i==0:
                array[i]=(exp(mat[i][i]*t),)+ mat[i][i+1:]
            elif i==m-1:
                array[i]=mat[i][:-1]+(exp(mat[i][i]*t),)
        except:
            print t, array[i][i], array[i][i]*t, 'Overflow here, asking for exp() of too large number'
    return (array)

def array2tuples(array):
    'make an array into a tuple of tuples that is non-mutable.  No longer have to use deepcopy'
    retobj=[]
    rows=cols=len(array[:,1])
    for i in range(rows):
        retobj.append(tuple(array[i,:]))
    return tuple(retobj)

class Model(object):
    """
    define an evolution model based on the rate matrix and nucleotide distribution
    """
    def __init__(self,R,prior):
        self.R=R
        #self.values, self.vectors=eigenvectors(R) #get the eigenval/vecs for when you need to exp()
        self.prior=[.35, .19, .25,.208]
        #self.D=array2tuples(make_diag(self.values)) #diagonal matrix of eigenvalues
        #self.U=Numeric.transpose(self.vectors)
        #self.Uinv=inverse(self.U)
        self.alphabet=['A','C','G','T']
        self.alpha=None
        self.beta=None
    def exp_matrix(self,t):
        #print 'calling exp_matrix'
        #matrix= Numeric.matrixmultiply(self.U, Numeric.matrixmultiply(exp_diag(self.D,t), self.Uinv)) # ie. R = U exp(D) U^-1
        #rowsum=sum(matrix[1,:])
        #matrix/=float(rowsum)
        if abs(sum(sum(matrix))-4.0)>.001:
            print "matrix not summing correctly:", abs(sum(matrix)-4.0)
            print matrix, t
        return matrix
    
    def hkyanalytic(self,i,j,t):
        "Implements the analytic solution to HKY85 evolution model"
        #A 0   C 1  G 2  T 3
        prior=self.prior #nucleotide equil. distribution
        transitions=[(1,3),(3,1),(0,2),(2,0)]
        purines=[0,2]
        Pi_pur=float(prior[0]+prior[2])
        pyrimidines=[1,3]
        Pi_pyr=float(prior[1]+prior[3])
        mu,kappa=self.alpha, self.beta/float(self.alpha) #define a ratio rather than the two params.
        #so many repeats of code here, not very well done.  Oh well.  Will fix later...
        if i==j: #stay same nucleotide
            if i in purines:
                return prior[j]+prior[j]*((1/Pi_pur) -1)*exp(-mu*t)+((Pi_pur-prior[j])/Pi_pur)*exp(-mu*t*(1+Pi_pur*(kappa-1)))
            elif i in pyrimidines:
                return prior[j]+prior[j]*((1/Pi_pyr) -1)*exp(-mu*t)+((Pi_pyr-prior[j])/Pi_pyr)*exp(-mu*t*(1+Pi_pyr*(kappa-1)))

        if (i,j) in transitions:
            if j in purines:
                return prior[j]+prior[j]*((1/Pi_pur)-1)*exp(-mu*t)-(prior[j]/Pi_pur)*exp(-mu*t*(1+Pi_pur*(kappa-1)) )
            if j in pyrimidines:
                return prior[j]+prior[j]*((1/Pi_pyr)-1)*exp(-mu*t)-(prior[j]/Pi_pyr)*exp(-mu*t*(1+Pi_pyr*(kappa-1)) )
        else: #(i,j) is a transversion
            return prior[j]*(1-exp(-mu*t))
            
    def prob(self,a,b,t): #note to self, this function is never called, I don't think...
        """
        probability that a character 'a' evolves into 'b' under time t, given that it is in state 'a'
        """
        print 'Calling prob forom inside matrix_exp module'
        return self.exp_matrix(t)[a,b]
    
def add_logs(x,y):
    "A fast way to add logarithms without having to use the exp() function"
    if not x==0 and not y==0:
        return x+log(1+exp(y-x))
    elif x==0 and y==0:
        print 'problem with log values'
        return None
    elif x==0:
        return y
    elif y==0:
        return x
    
def log_prob(data, model,t):
    """
    Gives the log-probabilty of an alignment given a model of evolution and a time period
    """
    seq1,seq2=tuple(data.keys())
    prob=model.exp_matrix(t)
    M=range(len(data[seq1]))
    logprob=0

    #convert letters to numbers...
    for position in M:
        a,b=model.alphabet.index(data[seq1][position]), model.alphabet.index(data[seq2][position])
        logprob+=log(prob[a,b])
    return logprob


def make_model(data,alpha,beta):
    """
    A helper function, basically do a few model-making steps in one
    """
    R,priors=get_HKY85_matrix(data,alpha,beta)
    model=Model(R,priors)
    model.alpha, model.beta=alpha,beta
    return model
def f(k,t):
    '''
    constructs a model with k=t, and evaluates the probability of the alignment with time=t
    '''
    temp_model=make_model(data, k,1)
    return log_prob(data, temp_model,t)
def hessian(x,y):
    """
    computes the 2-dim Hessian of f, at x,y
    """
    h=.001
    fxx=(f(x+2*h,y)-2*f(x+h,y) +f(x,y)) /h**2
    
    fyy=(f(x,y+2*h)-2*f(x,y+h) +f(x,y)) /h**2
    
    fxy=fyx= (f(x+h,y+h)-f(x+h,y)-f(x,y+h)+f(x,y)) /h**2
    return 1
    #return Numeric.array([[fxx,fyx],[fxy, fyy]])

def grad(x,y):
    '''
    gradient vector of f at x,y
    '''
    h=.001
    fx=(f(x+h,y)-f(x,y) )/ h
    fy=(f(x,y+h)-f(x,y))/h
    return 1
    #return Numeric.array([fx,fy])

def newton2(x,y, tolerance):
    '''
    A two-dimension Newton-Raphson optimization scheme
    '''
    change=100
    while change>tolerance:
        
        fx=f(x,y)
        Hinv=inverse(hessian(x,y))
        gradi=grad(x,y)
        diff=[1,2]
       #diff=Numeric.matrixmultiply(Hinv, gradi)
        x-=diff[0]
        y-=diff[1]
        new=f(x,y)
        change=abs(fx-new)
    return x,y,new

    
    
### The homework assignment: ###
### First part ###

#R_model=make_model(data, .1,.025)
#R_model.priors=[.3,.2,.2,.3]
##val=.01
##x=[]
##y=[]
##while val<=20:
##    x.append(val)
##    y.append(log_prob(data, R_model, val))
##    val+=.01
    
##r.pdf('dros_align.pdf')
##r.plot(x,y, main='Likelihood of Alignment over Time')
##r.dev_off

### Second Part, optimize k and t simultaneously ###

##kHat, tHat, val=newton2(5,.2,.001)
##print kHat, tHat, val
##        
    





