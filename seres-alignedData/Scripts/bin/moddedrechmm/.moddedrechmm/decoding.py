#!/usr/bin/python
from numpy import *
import sys,time,random
import copy
minimum=30 #min length of recombinant region
maxGroupSize = 20 #largest set of breakpoints to be considered together
buffer = 5
# NB posteriors is assumed to be a global variable, a numpy array
# for arg in sys.argv:
#     if '-c' in arg:
#         minimum = int(arg.replace('-c',''))

def find_val(array,value):
    K,M,min = shape(array)
    for i in range(K):
        for e in range(min): #array is assumed to have 3rd dim of 'minimum'
            if array[i,M-1,e] == value:
                return i,M-1,e

def traceback(startPoint,pointers):
    points = [startPoint]
    while 1:
        current_point = points[-1]
        if current_point:
            points.append(pointers.get(current_point))
        else:
            points.remove(None)
            break
    return [point[0] for point in points]


def get_path(bin_vector, breakpoints):
    'from a binary vector and a set of breakpoints (these vectors must be equal length), extract the unique state path'
    newBreaks = []
    for i,val in enumerate(bin_vector):
        if val:
            newBreaks.append(breakpoints[i])
    state_path = []
    for col in range(len(posteriors[0,:])):
        if not col: # first column, initialize with max in 0th column
            state_path.append(biggest(posteriors,col))
        elif col in newBreaks or col<breakpoints[0] or col>breakpoints[-1]:
            state_path.append(biggest(posteriors,col))
        else: #col is not a breakpoint, so we remain in the same state
            state_path.append(state_path[-1])
    return state_path

def get_path2(bin_vector, breakpoints):
    start, end = breakpoints[0],breakpoints[-1]
    'from a binary vector and a set of breakpoints (these vectors must be equal length), extract the unique state path'
    newBreaks = []
    for i,val in enumerate(bin_vector):
        if val:
            newBreaks.append(breakpoints[i])
    #we initialize the statepath as having a buffer on the left side...
    state_path = [ biggest(posteriors,start-5) for i in range(buffer) ]
    for col in range(start,end+5):
        if col in newBreaks:
            state_path.append(biggest(posteriors,col))
        else: #col is not a breakpoint, so we remain in the same state
            state_path.append(state_path[-1])
    #print 'path for', bin_vector, state_path
    return state_path


def get_score(state_path,breakpoints):
    'returns the sum of posterior probabilities of a state path'
    #print 'getting score for ', breakpoints
    start = breakpoints[0]-5
    end = breakpoints[-1]+5
    try:
        assert len(state_path) == len(range(start,end))
    except:
        print 'indexing error! statepath length:', len(state_path), 'length of start, end:', end-start
    retVal = 0
    for index,val in enumerate(range(start,end)):
        try:
            retVal += posteriors[state_path[index], val]
        except:
            print breakpoints, 'was not verifiable'
            sys.exit()
    return retVal
    #return sum([posteriors[state,column] for column,state in enumerate(state_path)])

def get_decision_array(r):
    mat=[[1],[0]]
    while len(mat)!=2**r:
        newlist=[]
        for i,row in enumerate(mat):
            newlist.append(row+[1])
            row.append(0)
        mat+=newlist
    return array(mat)

def list_one(N,i):
    lis = []
    for j in range(N):
        lis.append(i>>j&1)
    return lis

def primitive_decoding(path):
        ##### primitive breakpoint cleaning-up.  Really we should do this with the posterior dist. ###
    breakpoints = []
    for i,entry in enumerate(path):
        if i==0:
            continue
        if entry!=path[i-1]:
            breakpoints.append(i)
    change = True
    thru = 0
    while change:
        breakpointCopy = copy.deepcopy(breakpoints)
        thru += 1
        change  = False
        for i,point in enumerate(breakpoints):
            if i==0:
                if point<minimum:
                    breakpoints[i]=-1
                    change = True
            elif abs(point-breakpointCopy[i-1]) < minimum:
                breakpoints[i]=-1
                change = True
        while breakpoints.count(-1)!=0:
            breakpoints.remove(-1)
    return breakpoints

def biggest(array,col):
    max_index=-1
    max_value=0
    rows, columns=shape(array)
    #print rows, columns
    for i in range(rows):
        if array[i,col]>max_value:
            max_index=i
            max_value=array[i,col]
    return max_index
def get_initial_breaks(posteriorArray,opt='None',breaks=[]):
    if type(posteriorArray)==list:
        path=posteriorArray
    elif opt=='shortpath':
        if breaks!=[]:
            pass
    else:
        rows,cols=shape(posteriorArray)
        path=[biggest(posteriorArray,0)]
        # just build the state path naively, by posterior matrix:
        for column in range(1,cols):
            path.append(biggest(posteriorArray,column)); #print 'path remains same'
    pathBreaks = []
    for i,entry in enumerate(path):
        if i==0:
            continue
        if entry!=path[i-1]:
            pathBreaks.append(i)
    return pathBreaks


def decoding2(posteriorArray, log=False):
    final_breaks = []
    initial_breakpoints = get_initial_breaks(posteriorArray)
    if log:
        print 'Our first guess at bps (max at each position):', initial_breakpoints
    groups = find_bad_groups(initial_breakpoints)
    if log:
        print 'Groups of close breakpoints:', groups
    for group in groups:
        if len(group)>maxGroupSize:
            sys.stderr.write('One group is very large, may slow down computations...size: '+str(len(group))+'\n')
            continue
        if log:
            print 'Current group under consideration:', group
        if len(group) == 1:
            final_breaks.append(group[0])
            if log:
                print '\t Group of length one, not an issue.  Continue'
            continue
        elif group[0]<minimum or len(posteriors[1,:])-group[-1]<minimum:
            for index,value in enumerate(group):
                if value<minimum or len(posteriors[1,:])-value<minimum:
                    group[index] = -1
            while group.count(-1)>0:
                group.remove(-1)
            if group==[]:
                continue
        scoreDict = {} # we evaluate each group separately
        #this dictionary stores the scores of each combination of breakpoints
        L = len(group)
        N = 2**len(group)
        for i in xrange(N): #search over all possible (!) combinations of breakpoints, now memory-efficient!
            bin_vector = list_one(L,i) #this gives the ith binary vector of length L
            try:
                assert len(bin_vector) == len(group)
            except:
                print 'problem: bin_vector not equal length to bp list!'
                sys.exit(1)
                
            #filtering out those which have breaks too close together
            if good_path(bin_vector,group):
                #this is the key step, so we break it apart a bit:
                #in theory, a binary vector should map to exactly one path, which is the path from the first bp in 'group' to the end, where breakpoints are only allowed at those which are specified by bin_vector
                path = get_path2(bin_vector,group) 
                score = get_score(path,group)        
                if log:
                    print bin_vector, 'has score', score
                scoreDict[tuple(bin_vector)] = score
            else:
                continue

        #now recover the maximal statepath:
        for key in scoreDict.keys():
            if scoreDict[key]==max(scoreDict.values()):
                maxBreaks = get_initial_breaks(get_path2(key,group),opt='shortpath',breaks=group)
                for i,value in enumerate(maxBreaks):
                    maxBreaks[i]= value-buffer+group[0]
                    
                if log:
                    print key, 'has full path:', maxBreaks
                if not maxBreaks == []:
                    for index,val in enumerate(maxBreaks):
                        if val<group[0] or val>group[-1]:
                            maxBreaks[index]=-1
                while maxBreaks.count(-1)>0:
                    maxBreaks.remove(-1)
                    
                final_breaks += maxBreaks
                if log:
                    print key, 'is maximal, with path:', maxBreaks,'\n'
                break
    #test
    for val in final_breaks:
        if final_breaks.count(val)>1:
            sys.stderr.write('Warning: breakpoints have duplicate values!')
    #one final check:
    if len(final_breaks)>0:
        if final_breaks[0]<minimum:
            final_breaks.pop(0)
    sys.stderr.write('Final decoded breakpoints: '+str(final_breaks)+'\n')
    
    return final_breaks
        
def good_path(bin_vector, breaks):
    " boolean if bin_vector defines a good state path of values from breaks"
    newBreaks = []
    for i,val in enumerate(bin_vector):
        if val:
            newBreaks.append(breaks[i])
    for i, point in enumerate(newBreaks):
        if point < minimum:
            return False
        if len(posteriors[1,:]) - point <minimum:
            return False
        if not i:
            continue
        if abs(point - newBreaks[i-1])<minimum:
            return False
    return True

def find_bad_groups(breaks):
    'finds groups of breakpoints which have members within minimum'
    retList = [[]]
    for i, point in enumerate(breaks[:-1]):
        if breaks[i+1]-point <= minimum:
            retList[-1].append(point)
        else:
            retList[-1].append(point)
            retList.append([])
            
    if retList[-1] == []: #the 2nd-last breakpoint was not close to the last one
        if log:
            print 'examining last bp'
        if len(breaks)>0:
            if len(posteriors[0,:]) - breaks[-1] < minimum :
                retList[-1] += [breaks[-1],len(posteriors[1,:])-1] # a bit cryptic, but basically just deciding whether or not to keep the last breakpoint
            else:
                retList[-1].append(breaks[-1]) # it is a 'good' lone breakpoint, keep it
   
    else: # the last group was never terminated!
        #sys.stderr.write('Last breakpoint could be significant: '+str(breaks[-1])+' ' + str(retList)+'\n')
        retList[-1].append(breaks[-1])
    for subList in retList:
        if len(subList)<1:
            retList.remove(subList)
 #   sys.stderr.write('groups: '+str(retList)+'\n')
    return retList
    
# try:
#     for line in open(sys.argv[1]).readlines():
#         if '=matrix(c(' in line:
#             nrows = int(line[line.index('nrow=')+5:-2])
#             numbers2parse = line[line.index('c(')+2: line.index(')')]
#             bigList = [ float(num) for num in numbers2parse.split(',')]
#             newArray = [ [] for i in range(nrows) ]
#             lenRow = len(bigList)/nrows
#             for i in range(nrows):
#                 newArray[i] = bigList[i*lenRow:(i+1)*lenRow]
#             posteriors = array(newArray)
# except:
#     sys.stderr.write('Problem opeing inputfiles !\n')





# old decoding function:
def decoding1(posteriors, log=False):
    rows,cols=shape(posteriors)
    path=[biggest(posteriors,0)]
    #first, just build the state path naively, by posterior matrix:
    for column in range(1,cols):
        path.append(biggest(posteriors,column)); #print 'path remains same'
    pathBreaks = []
    for i,entry in enumerate(path):
        if i==0:
            continue
        if entry!=path[i-1]:
            pathBreaks.append(i)
    if 1:
        print 'initial brks:', pathBreaks
    path_changed = True
    times_thru_loop = 0
    while path_changed:
        if log:
            print 'Current breakpoints:', pathBreaks
        if times_thru_loop > 1000:
            print 'seemingly infinite loop averted!!!'
            return primitive_decoding(path)
        times_thru_loop += 1
        path_changed = False
        for i,point in enumerate(pathBreaks):
            if path_changed:
                break
            curState = path[point]
            if i==len(pathBreaks)-1:
                if cols-point<minimum:
                    print point, 'is within', minimum, cols
                    path_changed = True
                    pathBreaks.remove(point)
                    if log:
                        print 'point near end of alignment!'
                    for index in range(point,cols):
                        path[index]=path[point-1]
                        
            elif pathBreaks[i+1]-point<minimum:
                path_changed = True
                if log:
                    print 'encountered short region', pathBreaks[i+1],point
                if path[pathBreaks[i]+2]==path[point-1] or rows==2:# same region flanking the small-region
                    if 1:
                        print pathBreaks[i],pathBreaks[i+1], 'removed from breakpoints'
                    if log:
                        print path[pathBreaks[i+1]], path[point-1]
                    for index in range(point, pathBreaks[i+1]):
                        path[index] = path[point-1]
                        pathBreaks[i]=pathBreaks[i+1] = -1

                    for count in range(pathBreaks.count(-1)): #this is clumsy...
                        pathBreaks.remove(-1)
                else: #different regions flanking: which 'gets' the new region??
                    if log:
                        print 'dispute between:', path[pathBreaks[i]+2], path[point-1]
                    disputed_array = copy.deepcopy(posteriors[:,point:pathBreaks[i+1]])
                    if log:
                        print disputed_array
                    disputed_array[curState,:] = 0.0
                    for j, column in enumerate(disputed_array[0,:]):
                        absIndex = point+i
                        path[absIndex] = biggest(disputed_array,column)
    if log:
        print 'final path:', path
    sys.stderr.write('decoded breakpoints:'+ str(pathBreaks))
    return pathBreaks
#print good_path([1,0,1],[200,400,599])
#print 'breakpoints=c'+str(tuple(decoding2(posteriors)))
def matrix_max(matrix):
    shapeTuple = shape(matrix)
    num_entries = 1
    for i in shapeTuple:
        num_entries *= i
    return max(reshape(matrix,(1,num_entries))[0])

def decoding3(posteriors,cutoff):
    K,M = shape(posteriors)
    U=zeros((K, M ,cutoff+1))
    pointers = {}
    for m in range(M):
        for e in range(min(m,cutoff)+1):
            for i in range(K):
                if int(m>0 and m<cutoff and e==0):
                    MatrixMax = max( U[:,m-1,m-1])
                else:
                    MatrixMax = 0
                max_choices = [0, \
                              int(m>0) * U[i,m-1,e], \
                              int(m>0 and e>0) * U[i,m-1,e-1], \
                              int(m>=cutoff and e==0) * max(U[:,m-1,cutoff]), \
                              int(m>0 and m<cutoff and e==0) * MatrixMax \
                              ] #MatrixMax = matrix_max([U[:,m-1,kp] for kp in range(m)])\
                max_value = max(max_choices)
                came_from = max_choices.index(max_value)
                if came_from:
                    if came_from == 1:
                        pointers[i,m,e] = (i,m-1,e)
                    elif came_from == 2:
                        pointers[i,m,e] = (i,m-1,e-1)
                    elif came_from == 3:
                        state = argmax(U[:,m-1,cutoff])
                        pointers[i,m,e] = (state, m-1,cutoff)
                    elif came_from == 4:
                        state = argmax(U[:,m-1,m-1])
                        pointers[i,m,e] = (state, m-1,m-1)
                        
                    
                U[i,m,e] = posteriors[i,m] + max_value
                #pointers[i,m,e] = max_choices.index(
                #print 'choices were:', max_choices
                #print i,m,e, '\t the max value was:', U[i,m,e] - posteriors[i,m]
                #print ' and the new cell = ', U[i,m,e]
    return U, pointers

# a small test case for debugging:
option = 'testa'
if option == 'test':
    posteriors = array([ [.25, .25, .25, .25, .65 ,.65, .75], \
                     [.25, .01, .01, .01, .15, .15, .1],\
                     [.5, .74, .74, .74, .2, .2,.15]])


# now = time.time()
# U,pointers = decoding3(posteriors)
# K, M = shape(posteriors)
# Max_U = matrix_max(U[:,M-1,:])
# print Max_U
# print matrix_max(U)
# path= traceback(find_val(U,Max_U),pointers)
# sys.stderr.write('Posterior decoding DP took '+str(time.time() - now)+' seconds\n')
# sys.stderr.write('Posterior decoded breakpoints: '+ str(get_initial_breaks(path))+'\n')
# print 'breakpoints=c'+str(tuple(get_initial_breaks(path)))+'\n'
            


        

