                  ### Main Tree Optimization Script  ###
#from Numeric import *
import time
from math import log, exp
import copy
import random
from felsenstein import *
from messages import *


global sigma,M
sigma=[0,1,2,3] # for DNA, anyway



def optimize_tree(current_tree,sequence_array, times, log=False):
    if log:
        print 'original likelihood of tree:         ',current_tree.likelihood()
    #like.append(current_tree.likelihood())
    while times!=0:
        now=time.time()
        seqs=copy.copy(sequence_array)
        nodes=current_tree.nodes
        info=return_object(.01, current_tree)
        #data_constant=get_data_constant(sequence_array)
        W,b_l=optimize_links(current_tree,current_tree.M)
        info.W=W
        #print b_l
        for i in range(nodes):
            W[i,i]=-1000
        info.max_spanning=max_span=max_span_tree(W,b_l)
        for i in range(nodes):
            max_span[i,i]=0
        #print max_span
        info.adj=max_bifurcating=make_trivalent(max_span)
        info.new_prob=sum(multiply(max_span, W))
        then=time.time()-now
        if log:
            print 'maximizing the tree took',then,'seconds'
        if log:
            print 'maximized tree: '
            tree2dot(max_bifurcating,'max')               
        times-=1

        ### if Iterating more than 1 time: ###
        if times!=0:
            next_tree=tree(info.adj, seqs)
            if log:
                print 'Building a new tree, and getting expected counts again'
            next_tree.S, num=get_expected_counts(current_tree)
            next_tree.posteriors=current_tree.posteriors
            change=next_tree.likelihood()-current_tree.likelihood()
            if change>0:
                if log:
                    print 'Tree Improved, new likelihood: ',next_tree.likelihood()
            elif abs(change)<.001:
               if log:
                   print 'Tree likelihood changed by almost 0'
               if abs(change)==0:
                   return info
            else:
                if log:
                    print 'Tree likelihood decreased: ', next_tree.likelihood(), 'by', change
            
            current_tree=next_tree
            del next_tree
    return info

def get_data_constant(seqs):
    constant=0
    rows=len(seqs[:,1])
    cols=len(seqs[1,:])
    for i in range(rows):
        for j in range(cols):
            constant+=log(prior(main_tree,int(seqs[i,j])))
    return constant

# For now:.... ###
baby_alignment=array([[3,0,2,1,1,3,2,1,1],[2,0,2,1,0,1,3,0,0],[1,0,0,3,0,1,3,0,0],[1,0,0,3,2,1,1,2,0]])
baby_tree1=array([[0,0,0,0,0,0,2.5],[0,0,0,0,.25,0,0],[0,0,0,0,.25,0,0],[0,0,0,0,0,.5,0], [0,.25,.25,0,0,.25,0],[0,0,0,.5,.25,0,.25],[2.5,0,0,0,0,3,0]])
baby_tree0=array([[0,0,0,0,.5,0,0],[0,0,0,0,.5,0,0],[0,0,0,0,0,.5,0],[0,0,0,0,0,.5,0],
                    [.5,.5,0,0,0,0,2],[0,0,.5,.5,0,0,3],[0,0,0,0,2,3,0]])
do=False
if do:
    like=[]
    main_tree=tree(baby_tree1, baby_alignment)
    before=1
    for col in range(4,9):
        before*=main_tree.prob_column(col)
    print 'scaled original:', before
    main_tree.posteriors=[0,0,0,0,1,1,1,1,1]
    main_tree.S,num=get_expected_counts(main_tree)
    info=optimize_tree(main_tree,baby_alignment,1)
    scaled_tree=tree(info.adj, baby_alignment)
    after=1
    for col in range(4,9):
        after*=scaled_tree.prob_column(col)
    print 'scaled new:', after


    sub=tree(baby_tree1, baby_alignment[:,4:])
    print 'original likelihood: ', exp(sub.likelihood())
    sub.posteriors=[1,1,1,1,1]
    sub.S, num=get_expected_counts(sub)
    sub_info=optimize_tree(sub, baby_alignment[:,4:], 1)
    restricted_tree=tree(sub_info.adj, baby_alignment[:, 4:])
    print 'new likelihood: ', exp(restricted_tree.likelihood())

    print "Doing first for cols of alignment"
    
    main_tree=tree(baby_tree0, baby_alignment)
    before=1
    for col in range(4):
        before*=main_tree.prob_column(col)
    print 'scaled original:', before
    main_tree.posteriors=[1,1,1,1,0,0,0,0,0]
    main_tree.S,num=get_expected_counts(main_tree)
    info=optimize_tree(main_tree,baby_alignment,1)
    scaled_tree=tree(info.adj, baby_alignment)
    after=1
    for col in range(4):
        after*=scaled_tree.prob_column(col)
    print 'scaled new:', after


    sub=tree(baby_tree0, baby_alignment[:,:4])
    print 'original likelihood: ', exp(sub.likelihood())
    sub.posteriors=[1,1,1,1,1]
    sub.S, num=get_expected_counts(sub)
    sub_info=optimize_tree(sub, baby_alignment[:,:4], 1)
    restricted_tree=tree(sub_info.adj, baby_alignment[:,:4])
    print 'new likelihood: ', exp(restricted_tree.likelihood())



def oscar_test(d,d_seqs):
    a=tree(d, d_seqs)
    a.posteriors=[1,1,1,1,1]    
    a.S,num=get_expected_counts(a)
    info=optimize_tree(a, d_seqs, 3,log=False)
