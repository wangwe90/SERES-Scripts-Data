#!/usr/bin/python
from numpy import *
import sys
from messages import get_relationships
f=open(sys.argv[1])
lines=f.readlines()
max_i=0
D_Dict={}

class dot2newickTree(object):
    def __init__(self, root,array):
        self.root=root
        self.adj=array
        self.parent={}
        self.children={}
        self.sibling={}

def getroot(array):
    i,j=shape(array)
    for row in range(i):
        if i-list(array[row,:]).count(0)==2:
            return row
tree_index=0
regions=[]
trial_index = -1
trialDict = {}
for line in lines:
    if 'Region' in line:
        regions.append(line.strip()); continue
    if 'Trial' in line:
        trial_index+=1
    trialDict[tree_index] = trial_index
    if '}' in line:
        tree_index+=1
    if not '-' in line:
        continue
    if '{' in line:
        i=int(line[line.index('{')+1:line.index('-')])
    else:
        i=int(line[:line.index('-')])

    j=int(line[line.index('-')+2:line.index('[')])
    max_i=max(i,max_i,j)
    val=float(line[line.index('=')+1:line.index(']')])
    D_Dict[tree_index,i,j]=val
D=[zeros((max_i+1,max_i+1)) for i in range(tree_index)]
for tree in range(tree_index):
    for i in range(max_i+1):
        for j in range(max_i+1):
            if D_Dict.has_key((tree,i,j)):
                D[tree][i,j]=D_Dict[tree,i,j]
            elif D_Dict.has_key((tree,j,i)):
                D[tree][i,j]=D_Dict[tree,j,i]
def newick(node):
 #   print 'calling newick(', node

    retString=''
    if children[node]:
#        print node,'has children:', children[node]
        retString+='('
        for child in children[node]:
            retString+=newick(child)+':'+str(dist[node,child])
            if child==children[node][-1]:
                retString+=')'
            else:
                retString+=','
    else:
        retString=nameList[node]
    return retString

#try out stuff:
howMany=tree_index
nameList=sys.argv[2:]
current_trial = -1
for i in range(howMany):
    if trialDict[i]>current_trial:
        print '\nTrial '+str(trialDict[i])+' :'
        current_trial = trialDict[i]
    parent,children,sibling=get_relationships(dot2newickTree(getroot(D[i]),D[i]))
    dist=D[i]
    print regions[i]
    print newick(getroot(D[i]))+';'
