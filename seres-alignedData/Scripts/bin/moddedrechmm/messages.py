from numpy import *
import time
from math import log, exp
from copy import deepcopy
import random
from matrix_exp import *
from felsenstein import *
import sys
import os
sys.path.append(sys.path[0]+'/memory_tree')
# hard coded location of memory_tree module:
sys.path.append('/nfs/users/oscar/drop/recHMMclean/memory_tree')
try:
    import memory_tree
except:
    print 'Could not import C++ extension.  Try recompiling/swig steps.  '
    sys.exit(1)


d_seqs=array([[1,2,3,0,0],[1,2,3,2,1],[2,1,1,1,3],[2,1,0,3,2],[2,2,1,1,1]])
d=array([
       [ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  1.],
       [ 0.,  0.,  0.,  0.,  0.,  0.,  1.,  0.],
       [ 0.,  0.,  0.,  0.,  0.,  0.,  1.,  0.],
       [ 0.,  0.,  0.,  0.,  0.,  3.,  0.,  0.],
       [ 0.,  0.,  0.,  0.,  0.,  5.,  0.,  0.],
       [ 0.,  0.,  0.,  3.,  5.,  0.,  0.,  6.],
       [ 0.,  1.,  1.,  0.,  0.,  0.,  0.,  3.],
       [ 1.,  0.,  0.,  0.,  0.,  6.,  3.,  0.]])

sigma=range(4)



def olog(x):
    if x==0:
        return -inf 
    if x<0:
        print 'calling negative log in messages.py module'
    else:
        return log(x)

def add_logs(x,y):
    "A fast way to add logarithms"
    if not x==0 and not y==0:
        return x+olog(1+o_exp(y-x))
    elif x==0 and y==0:
        print 'problem with log values'
        return 3500
    elif x==0:
        return y
    elif y==0:
        return x

def Prob(tree,a,b,t):
    """
    define evolution model
    """
    alpha=.1
    return tree.model.hkyanalytic(a,b,t)
    
def prior(tree,a):
    #return .25
    return tree.model.prior[a]                
    
def is_even(x):
    return  not x%2

class tree(object):
    def __init__(self,D,seq_array):
        self.seqs=seq_array
        self.model=make_model(self.seqs, .039,.00756)
        seqs=seq_array
        self.M=len(self.seqs[1,:])
        self.sigma=range(4) #possible change, make rate-matrix dependent
        self.A=4
        self.D=D
        self.nodes=len(self.D[:,1])
        if is_even(len(D[:,1])):
            self.N=int(.5*(self.nodes+2))
        else:
            self.N=int(.5*(self.nodes+1))
        self.root=self.N+2
        if is_even(len(D[:,1])): #to put in extra node to make a rooted tree
            self.adj=alter(self)
        else:
            self.adj=self.D
        self.edges=list_edges(self.adj)
        self.parent, self.children, self.sibling=get_relationships(self)
        if is_even(len(D[:,1])):
            self.nodes+=1
        self.e, self.f,=Messages(self, log=False)#messages(self)

        self.posteriors=None

    def likelihood(self):
        '''
        calculate log-likelihood of a tree/alignment
        '''
        likelihood=0
        for m in range(self.M):
            sum=0
            for a in self.sigma:
                if 1+exp((olog(prior(self,a))+(self.f[m,self.root,a]))-sum)<0:
                    print 'less than 0 log request!'
                
                sum=add_logs(sum,olog(prior(self,a))+self.f[m,self.root,a])
                             
            likelihood+=sum
            
        return likelihood
            
    def relatives(self, node):
        children=list(self.children.get(node))
        parent=self.parent.get(node)
        if children and parent: #has children and parent
            children.append(parent)
            return children
        elif children and not parent: #has kids, but not parents (root)
            return children
        else: #has no children i.e. leaf node
            return [self.parent[node]]
    def prob_column(self,m):
        """
        P(observed column m | tree)
        """
        sum=0
        for a in sigma:
            sum=add_logs(sum,log(prior(self,a))+self.f[m,self.root,a])
        return sum
    
#     def prob_node(self, m, n, a):
#         """
#         P(x_n=a , position m
#         """
#         return self.f[m,n,a]+self.g[m,n,a]-self.prob_column(m)


    
#     def prob_joint_node(self,m,p,a,c,b):
        
#         """
#         P(x_p=a, x_c=b | obs[:,m])
#         """
#         if not (p,c) in self.edges:
#             if not (c,p) in self.edges:
#                 print 'calling joint node probability on non-tree edge!'
#         #check parent-child relationship:
#         if self.parent.get(p)==c:
#             temp2=a
#             a=b
#             b=temp2
#             temp=p
#             p=c
#             c=temp
#         root=self.root
#         s=self.sibling[c] #define siblings
#         return self.g[m,p,a]+olog(Prob(self,a,b,self.adj[p,c]))+self.f[m,c,b]+self.e[m,s,a]-self.prob_column(m) 

#BEGIN change by jsmith

#This function doesn't really work at all
# def tree2newick(tree, log=False):
#     Newick='('
#     queue=list(tree.children[tree.root])
#     letters='abcdefghijklmnopqrstuvwxyz'
#     while len(queue)!=0:
#         node=queue.pop()
#         if log:
#             print 'next off queue:', node
#         Newick+=str(letters[node])+':'+str(tree.adj[node, tree.parent[node]])
#         if log:
#             print 'string now:', Newick
#         if tree.children.get(node):
#             Newick+='('
#             queue+=tree.children[node]
#         else:
#             Newick+=')'
#     Newick+=';'
#     return Newick

#Replaced with these two, a bit of a hack but way better
def tree2newick(tree, taxa):
    newick_str = ''
    # alpha = list('abcdefghijklmnopqrstuvwxyz')
    # alpha.reverse()
    # newick_str += recursive_newick(tree, tree.root, alpha)
    newick_str += recursive_newick(tree, tree.root, taxa)
    newick_str += ';'
    return newick_str

def recursive_newick(tree, node, alphabet):

    #Get a list of children of the node
    children = list(tree.children[node])

    #Get the distance from this node to the parent
    dist_str = ''
    if(node != tree.root):
        dist_str = str(tree.adj[node, tree.parent[node]])

    #Base case, the node is a leaf...
    if len(children) == 0:
        #Just grab the next available leaf label and distance string
        return alphabet[node] + ':' + dist_str

    #Otherwise we recurse on all the children...
    child_newick_strs = [recursive_newick(tree, x, alphabet) for x in children]

    #...Take the result an make a coma separated list wraped in parentheses
    result = '(' + ','.join(child_newick_strs) + ')' 

    #...and put the distance on the end (if we have one) before returning
    if(dist_str != ''):
        result += ':' + dist_str
    return result

#END change by jsmith


        
def list_rels(i, i_array):
    'lists the relatives of a given node in an adjacency matrix'
    if i>len(i_array[:,1]):
        print 'Array size mismatch!'
    neighbors=[]
    if type(i_array)==list:
        print 'calling list_rels on list'
    for j,entry in enumerate(i_array[i,:]):
        try:
            if j in neighbors or entry==0 or i==j:
               continue
        except ValueError:
            print j
        else:
            neighbors.append(j)
    return neighbors
def list_edges(Tree):
    'returns a list of node-pairs in a tree (as matrix)'
    nodes=len(Tree[:,1])
    edges=[]
    for i in range(nodes):
        if i ==nodes-1:
            break
        for j in range(nodes):
            if Tree[i,j]==0 or i==j:
                continue
            if not (i,j)  in edges:
                if not (j,i) in edges:
                    edges.append((i,j))
    return edges
def get_power_set(self, log=False):
    return 1
    P_S=set()
    P_dict={}
    queue=[]
    for i in range(self.N):
        queue.append(self.parent[i])
        P_dict[i]=()
    while 1:
        root=False
        if log:
            print queue
            print P_S
        if queue==[]:
            break
        node=queue.pop(0)
        if log:
            print 'popped', node
        if not node in queue:
            queue.append(node)
            continue
        else:
            while node in queue:
                queue.remove(node)
            try:
                queue.append(self.parent[node])
            except:
                root=True

        if not root:
            kid1, kid2=tuple(self.children[node])
            kidlist=[kid1,kid2]
        else:
            P_S.add(tuple(range(self.N)))
##                kid1, kid2, kid3=tuple(self.children[node])
##                kidlist=[kid1,kid2,kid3]
        for kid in kidlist:
            if kid>=self.N:
                kidlist.remove(kid)
        if log:
            print tuple(list(P_dict.get(kid1))+list(P_dict.get(kid2))+ kidlist)
        P_S.add(tuple(list(P_dict.get(kid1))+list(P_dict.get(kid2))+kidlist))
        P_dict[node]=tuple(list(P_dict.get(kid1))+list(P_dict.get(kid2))+kidlist)
    return P_S
def alter(self):
    D=self.D
    root=self.root
    if len(D[:,1])==root:
        print 'changed something'
        root-=1
    nodes=self.nodes
    eps=.01    
    kids=list_rels(root,D)
    new=self.nodes
    adj=zeros((self.nodes+1, self.nodes+1))
    adj[0:nodes,0:-1]=D
    adj[root,new]=adj[new,root]=eps
    x,y=kids[1],kids[2]
    adj[new,x]=adj[x,new]=D[root, x]
    adj[new,y]=adj[y,new]=D[root,y]
    adj[root,x]=adj[x,root]=adj[root,y]=adj[y,root]=0
    return adj


def prob_obs(a,b):
    if a==4: #gap probability
        return .999
    if a==b:
        return .999
    else:
        return .00033
    
def get_relationships(tree):
    """
    returns vectors of children and parent relationships in rooted tree
    """
    children={}
    parent={}
    sibling={}
    relatives={}
    queue=[tree.root]
    covered_nodes=[tree.root]
    while 1:
        if queue==[]:
            break
        #print queue
        i=queue.pop(0)
        covered_nodes.append(i)
        y=list_rels(i,tree.adj)
        #print 'relatives of',i, 'are', y
        x=deepcopy(y)
        for node in y:
            if node in covered_nodes:
                x.remove(node)
                #print 'removed', node, 'as a possible child of ',i
                #if parent[i]!=node:
                 #   print 'funny business'
            else:
                parent[node]=int(i)
                #print 'added', int(i),'as parent of', node
        children[i]=tuple(deepcopy(x))
        if len(x)==2:
            boy,girl=x[0],x[1]
            sibling[boy]=girl
            sibling[girl]=boy
        #print x,'added as children of', i
        queue+=x
        
    return parent,children,sibling
        
        
    
def Messages(tree, log=True):
    now=time.time()
    #seqs assumed to be global
    root=tree.root
    M=len(tree.seqs[0,:])
    N=tree.N
    sigma=[0,1,2,3]
    A=len(sigma)
    nodes=tree.nodes
    f=zeros((M,nodes,A))
   
    e=zeros((M,nodes, A))
    for m in range(tree.M):
        covered_nodes=[]
        covered_edges=[]
        queue=[]
        if log:
            print 'pos',m
        ###   Declare leaf nodes ###
        for n in range(N):
            for a in sigma:
                f[m,n,a]=olog(prob_obs(tree.seqs[n,m],a)) #probability of observing something given xn = a
            covered_nodes.append(n)
            j=tree.parent[n]
            queue.append(j)
        while 1:
            if log:
                print 'cue:', queue
            if len(queue)==0:
                break
            p=queue.pop(0)
            kids=deepcopy(tree.children[p])
            if p in queue:
                #ask if i was added to the queue twice, i.e. both incoming messages have been made
                queue.remove(p)
            else:
                queue.append(p)
                continue
            if log:
                print p,'next in upward queue'
            
            if log:
                print kids
            for a in sigma:
                f[m,p,a]=0
                for c in kids:
                    sum=0
                    for b in sigma:
                        sum=add_logs(sum,olog(Prob(tree,a,b,tree.adj[p,c]))+f[m,c,b])
                    if log:
                        print sum
                    e[m,c,a]=sum
                    f[m,p,a]+=sum
                    if log:
                        print 'added ',c,p
            if f[m,p,a]==0:
                print 'an entry of f is zero...'
            new_parent=tree.parent.get(p)
            if new_parent:
                queue.append(new_parent)
            covered_nodes.append(p)
            covered_edges.append((c,p))
##        for mp in range(M):
##            for p in range(nodes):
##                for a in sigma:
##                    if f[mp,p,a]==0 and p>=N:
##                        print 'an entry of f is zero...', mp,p,a
##                        print 'while in position', m
    ###  Downward Messages  ###
   #      if log:
#             print 'going downward now...'
#         for a in sigma:
#             g[m,root,a]=olog(prior(tree,a))
#         queue=list(deepcopy(tree.children[root]))
#         for p in range(nodes):
#             for a in sigma:
#                 if f[m,p,a]==0 and p>=N:
#                     print 'an entry of f is zero...', m,p,a
#         while 1:
#             if queue==[]:
#                 break
#             c=queue.pop(0)
#             if tree.children[c]:
#                 queue+=tree.children[c]
#             p=tree.parent[c]
#             s=tree.sibling[c]
#             if log:
#                 print 'considering the parent, child, sibling triplet: ', p,c,s
#             for b in sigma:
#                 g[m,c,b]=0
#                 for a in sigma:
#                    g[m,c,b]=add_logs(g[m,c,b], g[m,p,a]+olog(Prob(tree,a,b,tree.adj[c,p]))+e[m,s,a])
    #print 'message-passing took', (time.time()-now)/float(60), 'minutes'
    return e,f




def jukes_values(S,i,j):
    A=0.0
    B=0.0
    for a in sigma:
        for b in sigma:
            if a==b:
                A+=S[i,j,a,a]
            else:
                B+=S[i,j,a,b]
    return A+B
def expected_test(tree):
    all_good=True
    if type(tree)==tuple:
        S,M,nodes,A=tree
    else:
        S=tree.S
        M=tree.M
        nodes=tree.nodes
    for i in range(nodes):
        for j in range(nodes):
            if i==j:
                continue
            elif abs(jukes_values(S,i,j)-M)>.1:
                print jukes_values(S,i,j), i,j
                all_good= False
    #print 'reached end without trouble'
    return all_good
                  
def get_expected_counts(tree, log=False,dofast=True):
    if dofast:
        return get_Cpp_counts(tree)
#         except:
#             print "Doing Expected counts the long way: This is outdated now...for memory reasons."
    # if log:
#         print 'Computing expected counts the long way...'
#     now=time.time()
#     edges_done=[]
#     nodes, A, M=tree.nodes, tree.A, tree.M
#     expected_count_array=zeros((M,tree.nodes, tree.nodes, tree.A, tree.A))
#     def get_count(m,node,new,a,b,in_tree):
#         if in_tree:
#             return exp(tree.prob_joint_node(m,node,a,new,b))*tree.posteriors[m]
#         else:
#             sum=0
#             node_sum=0
#             xj=deepcopy(depth_dict[new])
#             for c in sigma:
#                 node_sum+=exp(tree.prob_node(m,xj,c))
#                 sum+=expected_count_array[m,node,xj, a,c]*exp(tree.prob_joint_node(m, xj,c,new, b)-tree.prob_node(m, xj, c))

#                 #tree.prob_joint_node(m,xj,c,new,b)*expected_count_array[m,node,xj, a,c]/tree.prob_node(m,xj,c)
#             if abs(node_sum-1)>.0001:
#                 print 'Node probability is not summing to 1, instead sums to:', node_sum
#             return sum*tree.posteriors[m]
    
    
#     for node in range(tree.nodes):
#         depth_dict={}
#         if log:
#             print 'starting point: ', node
#         queue=deepcopy(tree.relatives(node))
#         if log:
#             print 'queue', queue
#         for kid in queue:
#             depth_dict[kid]=node
#         while 1:
#             if queue==[]:
#                 break
#             new=queue.pop()
# ##            if (node,new) in edges_done:
# ##                if log:
# ##                    print 'saved some time, realized already computed', (node, new)
# ##                for m in range(M):
# ##                    for a in sigma:
# ##                        for b in sigma:
# ##                            expected_count_array[m,new, node,b,a]=expected_count_array[m,node, new,a,b]
                    
#             if log:
#                 print new, 'new off of queue'
#             to_add=deepcopy(tree.relatives(new))
#             if log:
#                 print ' all relatives:', to_add
#             to_add.remove(depth_dict[new])
#             if log:
#                 print 'but we remove', depth_dict[new]
#             queue+=to_add
#             if log:
#                 print 'relatives added: ', to_add
#             for rel in to_add:
#                 depth_dict[rel]=new
#             if (new,node) in tree.edges or (node,new) in tree.edges:
#                 tree_edge=True
#             else:
#                 tree_edge=False
#             if log:
#                 print 'computing a tree edge?:', ['not a tree edge','is a tree edge'][tree_edge]
#                 if not tree_edge:
#                     print 'using', depth_dict[new]
#                 print 'we compute: ', new,node
#             edges_done.append((new,node))
#             for m in range(tree.M):
#                 for a in sigma:
#                     for b in sigma:
#                         count=get_count(m,node, new,a,b,tree_edge)
#                         if not count <17 and not count>=17:
#                             print 'nan encountered in expected-count step'
#                             return None
#                         expected_count_array[m,node,new,a,b]=count


                    
#             ### Check sequence length invariant ###
#             slippy=False
#             if slippy:
#                 test_sum=0
#                 for m in range(M):
#                     for a in sigma:
#                         for b in sigma:
#                             test_sum+=expected_count_array[m,node,new,a,b]
#                 print test_sum, (node, new), ['not a tree edge','is a tree edge'][tree_edge]
                        
#                 #expected_count_array[m,:,:,:,:]*=tree.posteriors[0,m]
#             if log:
#                 print 'the current queue:', queue
#     print 'Python-computing expected counts took', (time.time()-now)/60.0, 'minutes'
#     #dummy=get_Cpp_counts(tree)
#     slow=time.time()-now
#     S=sum(expected_count_array, axis=0)
#     #S=expected_count_array

#     #test_counts(expected_count_array)
#    return S, slow
def test_counts(S):
    expected_count_array=S
    test_sum=0
    for node in range(9):
        for new in range(9):
            if node==new:
                continue
            test_sum=sum(S[new,node,:,:])
            if abs(test_sum-40)>.01:
                conj_test_sum=sum(S[node,new,:,:])
                if abs(conj_test_sum-40)>.01:
                    print 'a real probem pair! :', node, new
                    test_sum, conj_test_sum

def compare_expecteds(S,Sp):
    diffs=[]
    for i in range(oscar.nodes):
        for j in range(oscar.nodes):
            printing=False
            for a in sigma:
                for b in sigma:
                    if abs(S[i,j,a,b]-Sp[i,j,a,b])>.00001:
                        printing=True
                    diffs.append(S[i,j,a,b]-Sp[i,j,a,b])
    return diffs

def get_siblings(treeobj):
    sibs=[]
    for node in range(treeobj.nodes):
        if node==treeobj.root:
            sibs.append(0)
            continue
        sibs.append(treeobj.sibling[node])
    return sibs
def get_parents(treeobj):
    parents=[]
    for node in range(treeobj.nodes):
        if node==treeobj.root:
            parents.append(0)
            continue
        parents.append(treeobj.parent[node])
    return parents

def get_Cpp_counts(treeobj):
    sibling=get_siblings(treeobj)
    parent=get_parents(treeobj)
    sig=len(treeobj.sigma) # another name for sigma, the alphabet size.  this gets confusing
    S=[ [ [ [ 0 for i in range(sig)] for i in range(sig)] for i in range(treeobj.nodes)] for i in range(treeobj.nodes)]
    Sp=[ [ [ [ 0 for i in range(sig)] for i in range(sig)] for i in range(treeobj.nodes)] for i in range(treeobj.nodes)]
    mm=treeobj.M
    roott=treeobj.root
    adj=[]
    r,c=shape(treeobj.adj)
    for i in range(r): #make list list
        adj.append(list(treeobj.adj[i,:]))
    now=time.time()
    #print 'About to invoke Cpp module'

    ret=array(memory_tree.import_and_count(adj, mm, roott, treeobj.f,treeobj.e, parent, sibling, S, Sp, treeobj.posteriors)), time.time()-now
    S=None
    Sp=None
    return ret
run_example=False
if run_example:    
    oscar=tree(d,d_seqs)
    oscar.posteriors=[1 for i in range(oscar.M)]
    S,num=get_expected_counts(oscar, log=False, dofast=False)
    Sp, fast=get_Cpp_counts(oscar)
    diffs=[]
    for i in range(9):
        for j in range(9):
            for c in range(4):
                for b in range(4):
                    diffs.append(S[i,j,c,b]-Sp[i,j,c,b])
    from rpy import *
    r.pdf('differences.pdf')
    r.hist(diffs, breaks=1000, main="Differences in Expected Counts", xlab="Difference", ylab="Frequency")
    r.dev_off()


doing_check=False
if doing_check:
    baby_alignment=array([[3,0,2,1,1,3,2,1,1],[2,0,2,1,0,1,3,0,0],[1,0,0,3,0,1,3,0,0],[1,0,0,3,2,1,1,2,0]])
    baby_tree1=array([[0,0,0,0,0,0,2.5],[0,0,0,0,.25,0,0],[0,0,0,0,.25,0,0],[0,0,0,0,0,.5,0], [0,.25,.25,0,0,.25,0],[0,0,0,.5,.25,0,.25],[2.5,0,0,0,0,3,0]])

    baby_tree0=array([[0,0,0,0,.5,0,0],[0,0,0,0,.5,0,0],[0,0,0,0,0,.5,0],[0,0,0,0,0,.5,0],
                        [.5,.5,0,0,0,0,2],[0,0,.5,.5,0,0,3],[0,0,0,0,2,3,0]])

    bab0=tree(baby_tree0, baby_alignment)
    bab1=tree(baby_tree1, baby_alignment)
    
