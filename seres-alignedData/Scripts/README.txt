Intro:
This directory should contain everything you need in order to do two things:
    1. Run simulation studies on SERES local tree annotation
    2. Run emperical studies

Some of the software included depends on the ete3 python library for doing tree
operations. It is available at etetoolkit.org. It can be installed via anaconda
or via pip.

Setup:
The only thing you should need to do to get started is add the included bin
directory to your path.
export PATH="path_of_bin:$PATH"

File Conventions:
This software uses a few different kinds of files. 
    .fasta      Self explanatory, MSA
    .pos        Posterior log
    .walk       Log from resampler, shows where the resampler turns around.
                Used to translate positons back.
