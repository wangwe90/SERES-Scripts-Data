#!/bin/bash 

source parameters.sh

#Seed the Bash PRNG
RANDOM=$rngseed

for ((trialcount = 1; trialcount <= numtrials; trialcount++))
do
    #Make a directory for that particular trial and change to it
    trialdir=trial${trialcount}
    mkdir $trialdir

    #Next step, generate some genetrees, keep generating them untill we get some
    #that contain at least one topological break
    ms $numtaxa 1 -r $recombrate $numsites -T | tail -n +5 > \
        $trialdir/genetrees
    while ! (containstopobreak.py $trialdir/genetrees)
    do
        ms $numtaxa 1 -r $recombrate $numsites -T | tail -n +5 > \
            $trialdir/genetrees
    done

    #Change to the testing directory
    cd $trialdir

    #Evolve a sequence down the genetree
    numpartitions=`wc -l < genetrees`
    seq-gen -mHKY -l $numsites -p $numpartitions -s $mutrate -z $RANDOM \
        < genetrees > alignment.phylip

    #convert it to fasta
    phylip2fasta.pl alignment.phylip alignment.fasta
    rm alignment.phylip

    #run the resampler
    mkdir replicates
    seres-resample alignment.fasta -d replicates -l $lengthreplicates -n \
        $numreplicates -b $bias -s $RANDOM

    #Change back to the parent directory
    cd ..

    #Copy the posterior array script into the replicates directory
    cp scripts/posteriorsarray.qsub $trialdir/replicates/

done

#Cleanup
rm seedms
