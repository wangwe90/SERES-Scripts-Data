#!/bin/bash 

source parameters.sh

for ((trialcount = 1; trialcount <= numtrials; trialcount++))
do
    echo Trial Number is $trialcount
    #Get the name of each trial directory and change to it
    trialdir=trial${trialcount}
    cd $trialdir

    #Run the posterior averaging script on the replicates directory
    posterioravg.py replicates > seres.pos

    #Change back to the parent directory
    cd ..
done
