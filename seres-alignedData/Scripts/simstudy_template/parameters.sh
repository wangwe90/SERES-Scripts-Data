#Simulation and study parameters
numtrials=2
numtaxa=4
numsites=1000
recombrate="2"
mutrate="2"

#Resampling parameters
numreplicates=30
lengthreplicates=1000
bias="0.005"

#RecHMM parameters
numstates=3

#Seed for PRNG
rngseed=1
