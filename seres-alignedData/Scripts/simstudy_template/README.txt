This directory contains a template for performing a simulation study usign the
SERES local tree annotation toolchain.

The process goes like this:
    1. Edit the parameters.sh file to match the experiment you wish to perform.
       Be careful not too add too many individual replicates.
    2. Run setup.sh, this will generate a numbered directory for every trial.
       The root of each trial directory will contain a fasta alignment and the
       genetrees which generated it. It will also contain a subdirectory with
       all the SERES replicates.
    3. Run the launchjobs.sh script, this will submit all the HPCC jobs using
       qsub, as set up, this will be 930 (!!) individual jobs. 
    4. Run the consolidate.sh script, this will consolidate all the replicates to
       create a seres.pos in each trial directory.

After this, you are free to analyze the results however you like. You should
have two posterior logs for each trial, one from rechmm and one from the seres
method. The included posteriorscore.py script allows producing simple scores for
posterior distributions to determine how well they match the true local trees.
Look at its source for more info.

