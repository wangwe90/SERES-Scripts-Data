source parameters.sh

#Launch all the baseline jobs
qsub -t1-${numtrials} -V baseline.qsub -F "${numstates}"

#Launch all the individual seres replicate jobs
for ((trialcount = 1; trialcount <= numtrials; trialcount++))
do
    trialdir=trial${trialcount}
    cd $trialdir/replicates
    qsub -t 1-$numreplicates -V posteriorsarray.qsub -F "${numstates}" 
    cd ../..
done
