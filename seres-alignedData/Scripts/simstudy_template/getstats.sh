source parameters.sh

total_seres=0
total_baseline=0

echo "SERES-Error,Benchmark-Error" > stats.csv

for ((trialcount = 1; trialcount <= numtrials; trialcount++))
do
    trialdir=trial${trialcount}
    cd $trialdir

    #Calculate the scores
    seres_score=$(posteriorscore seres.pos genetrees)
    baseline_score=$(posteriorscore baseline.pos genetrees)

    echo $seres_score, $baseline_score >> ../stats.csv

    #Add them to the totals with bc
    total_seres=$(echo ${total_seres} + ${seres_score} | bc)
    total_baseline=$(echo ${total_baseline} + ${baseline_score} | bc)

    cd ..

done

#Take the averages
avg_seres=$(echo ${total_seres} / ${numtrials} | bc -l)
avg_baseline=$(echo ${total_baseline} / ${numtrials} | bc -l)

echo "The avg Baseline Score is: ${avg_baseline}" 
echo "The avg SERES Score is: ${avg_seres}" 
